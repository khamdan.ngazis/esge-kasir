<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BalanceModel extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
    }

    function getHistoryAccount($id, $date){
        $query = 'select tb.id as trx_id, ta.id , name , "" as transaction, case when trx_type = 1 then amount else "" end as kredit, 
        case when trx_type = -1 then amount else "" end as debet , trx_time , created_at , created_by 
        from trx_account ta left join trx_transaction_account tb on tb.trx_account_id = ta.id 
        where account_id = '.$id.' and date(created_at) = date("'.$date.'") order by trx_time';
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getHistoryBank($id, $date){
        $query = 'select tb.id as trx_id, ta.id , name , transaction, case when trx_type = 1 then amount else "" end as kredit, 
        case when trx_type = -1 then amount else "" end as debet , trx_time , created_at , created_by 
       from trx_bank ta left join trx_transaction_bank tb on trx_bank_id = ta.id 
       where bank_id = '.$id.' and date(created_at) = date("'.$date.'") order by trx_time';
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTrxbankDetail($id){
        $query = 'select * from trx_bank where id = '.$id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getAccountDetail($id){
        $this->db->select('*');
        $this->db->from('mtr_accounts');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function getBankDetail($id){
        $this->db->select('*');
        $this->db->from('mtr_bank_accounts');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function add_transaction_account($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income ){
        $query = "CALL add_transaction_account(".$user_id.",".$account_id.",".$balance.",1,
        '".$user_name."','".$transaction_name."','".$trx_time."',false,1,".$income.")";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function edit_transaction_account($trx_account_id,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income ){
        $query = "CALL edit_transaction_account(".$trx_account_id.", 0, ".$user_id.",".$account_id.",".$balance.",1,
        '".$user_name."','".$transaction_name."','".$trx_time."',false,1,".$income.")";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function delete_transaction_account($trx_account_id){
        $query = "CALL delete_transaction_account(".$trx_account_id.",0,1,false)";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function add_transaction_bank($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income , $trx_type , $jenis_transaksi){
        $query = "CALL add_transaction_bank(".$user_id.",".$account_id.",".$balance.",".$jenis_transaksi.",
        '".$user_name."','".$transaction_name."','".$trx_time."',false,1,".$income.", '".$trx_type."')";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function edit_transaction_bank($trx_account_id,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income,  $trx_type, $jenis_transaksi ){
        $query = "CALL edit_transaction_bank(".$trx_account_id.", 0, ".$user_id.",".$account_id.",".$balance.",".$jenis_transaksi.",
        '".$user_name."','".$transaction_name."','".$trx_time."',false,1,".$income.", '".$trx_type."')";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function delete_transaction_bank($trx_account_id){
        $query = "CALL delete_transaction_bank(".$trx_account_id.",0,1,false)";
        $result = $this->db->query($query)->row();
        return $result;
    }
}