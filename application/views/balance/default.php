<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="card-title"><b>Informasi Saldo</b></h3>
                        </div>
                        <div class="card-body">
                            <div class="card card-info">
                                <div class="card-body">
                                
                                    <table id="table_balance" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Saldo</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>           
                                            <?php
                                                $i = 1;
                                                foreach ($balance as $key => $value) {
						
                                            ?> 
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $value->name;?></td>
                                                <td>Rp <?php echo number_format($value->balance);?></td>
                                                <td> <a href="<?php echo BASE_URL."balance/detail/".$value->type."/".$value->id; ?>" class="btn btn-info btn-sm">History</a></td>
                                            </tr>
                                            <?php
                                                $i ++;
                                                }
                                            ?>
                                        </tbody>
                                    </table><br>
                                   
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
     $( document ).ready(function() {
        $("#balance").inputmask();
        $("#balance_edit").inputmask();
        $('#table_balance').DataTable({
            "paging": true,
            "lengthChange": false,
            "pageLength":10,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "oLanguage": {
                                "sInfo": "<b></b>",
                                "sInfoFiltered": "",
                                "sInfoEmpty":"<b></b>",
                    
                               "oPaginate": {
                                "sPrevious": "<i class='fa fa-angle-left'></i>",
                                "sNext":"<i class='fa fa-angle-right'></i>"
                            }
                    }
            });
        $('#balance_name').change(function() {
            $('#balance_type').html($('#balance_name').val())
            $('#form_balance_name').html($('#balance_name').val())
        })
     });
     
</script>