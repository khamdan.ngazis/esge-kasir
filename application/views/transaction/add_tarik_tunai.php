<form action="<?php echo BASE_URL; ?>transaction/save_tarik_tunai" method="POST" id="form_add">
<input type="hidden" name="action" value="add">
<div class="modal-header bg-info">
    <h4 class="modal-title">Tarik Tunai</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
    <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" placeholder="Masukan Nama" name="name" required>
        </div>
        <div class="form-group">
            <label >Bank</label>
            <select class="form-control" name="bank" required>
                <option value="">Pilih Bank</option>
                <?php foreach ($banks as $key => $value) {
                    echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                } ?>
            </select>
        </div>
        <div class="form-group">
            <label >Waktu Transaksi</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                </div>
                <input type="time" name="time" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label >Jumlah</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" class="form-control" id='balance' name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false">
            </div>
        </div>
        <div class="form-group">
            <label >Uang Keluar</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" class="form-control" id='income' data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false " name="income" required>
            </div>
        </div>
        <div class="form-group">
            <label >Jasa</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" class="form-control" id="service" name="service" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false " readonly>
            </div>
        </div>
        
    </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-info btn-sm">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
        $("#income").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });
    $( "#income" ).change(function() {
        var balance = $("#balance").val();
        var income = $("#income").val();
        var target = 'service';
        count_service( income,balance , target);
    })

    $( "#balance" ).change(function() {
        var balance = $("#balance").val();
        var income = $("#income").val();
        var target = 'service';
        count_service(income ,balance, target);
    })
</script>
