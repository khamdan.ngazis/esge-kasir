<?php
class user extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("UserModel");
        isLogin();
    }

    public function index(){
        $users = $this->UserModel->getAllUser();
        $data['users'] = $users;
        $data['link'] = "user";
        $data['link_child'] = $this->uri->segment('1');
		$data['content'] = 'user/default';
		$this->load->view('template',$data);
    }

    public function add(){
        $data['link'] = "user";
        $data['link_child'] = $this->uri->segment('1');
		$data['content'] = 'user/add';
		$this->load->view('template',$data);
    }

    public function edit(){
        $id = $this->uri->segment('3'); 
        if ($id == ""){
            redirect(BASE_URL.'user');
        }else{
            $user = $this->UserModel->getUser($id);
            if($user == NULL) {
                redirect(BASE_URL.'user');
            }else{
                $data['user'] = $user;
                $data['link'] = "user";
                $data['link_child'] = $this->uri->segment('1');
                $data['content'] = 'user/edit';
                $this->load->view('template',$data);
            }
        }
    }

    public function delete(){
        $id = $this->uri->segment('3'); 
        if ($id == ""){
            redirect(BASE_URL.'user');
        }else{
            $res = $this->MainModel->delete('mtr_users','id',$id);
            if($res == 1){
                $message = '<div class="alert alert-success alert-dismissable"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Success Delete Data</b>
                            </div>';    
                $sesion = array(
                    'message'  => $message
                );
                $this->session->set_flashdata($sesion);
                redirect(BASE_URL."user");
            }else{
                $message = '<div class="alert alert-danger alert-dismissable"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Failed Delete Data</b>
                            </div>';    
                $sesion = array(
                    'message'  => $message
                );
                $this->session->set_flashdata($sesion);
                redirect(BASE_URL."user");
            }
        }
    }

    public function save(){
        
        $Username = $this->input->post('Username');
        $Password = $this->input->post('Password');
        $Name = $this->input->post('Name');
        $Role = $this->input->post('Role');

        if ($this->input->post('Id') != ""){
            $Id = $this->input->post('Id');
            $data = array("username" => $Username,
                          "password" => hash("sha256", $Password),
                          "name" => $Name,
                          "Role" => $Role
            );
            $res = $this->MainModel->edit('mtr_users',$data,'id',$Id);

            if($res == 1){
                $message = '<div class="alert alert-success alert-dismissable"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Success Edit Data</b>
                            </div>';    
                $sesion = array(
                    'message'  => $message
                );
                $this->session->set_flashdata($sesion);
                redirect(BASE_URL."user");
            }else{
                $message = '<div class="alert alert-danger alert-dismissable"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Failed Edit Data</b>
                            </div>';    
                $sesion = array(
                    'message'  => $message
                );
                $this->session->set_flashdata($sesion);
                redirect(BASE_URL."user");
            }
                        
        }else{
            $data = array("username" => $Username,
                          "password" => hash("sha256", $Password),
                          "name" => $Name,
                          "Role" => $Role
            );
            $id = $this->MainModel->save('mtr_users',$data);
            if($id != 0){
                $message = '<div class="alert alert-success alert-dismissable"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Success Add Data</b>
                            </div>';    
                $sesion = array(
                    'message'  => $message
                );
                $this->session->set_flashdata($sesion);
                redirect(BASE_URL."user");
            }else{
                $message = '<div class="alert alert-danger alert-dismissable"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Failed Add Data</b>
                            </div>';    
                $sesion = array(
                    'message'  => $message
                );
                $this->session->set_flashdata($sesion);
                redirect(BASE_URL."user");
            }
        }

    }
}