<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DashboardModel extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
    }

    function getBalanceUser(){
        $query = "select a.id , a.`role` , a.name, 
                  IFNULL((select sum(amount * trx_type ) 
                  from trx_transaction where user_id = a.id and date(create_at) = curdate()), 0) as balance from mtr_users a 
                  ";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getAccountBalance(){
        $query = 'select id,name, balance , "bank" as type,
        CASE
            WHEN name = "BCA 0808" then "bg-info"
            When name = "BRI GIRO" then "bg-secondary"
	    When name = "BRI BRITAMA" then "bg-secondary"
            when name = "BNI" then "bg-warning"
            when name = "PERMATA Deny" then "bg-success"
            when name = "PERMATA Tati" then "bg-success"
            when name = "MANDIRI" then "bg-info"
            when name = "BJB" then "bg-default"
            when name = "RBR" then "bg-danger"
            when name = "TEKTAYA" then "bg-default"
            ELSE "bg-info" END as background
        from mtr_bank_accounts mba 
        union
        select id, name , balance, "lain" as type, CASE
        WHEN name = "BCA" then "bg-info"
        When name = "BRI" then "bg-secondary"
        when name = "BNI" then "bg-warning"
        when name = "PERMATA 1" then "bg-success"
        when name = "PERMATA 2" then "bg-success"
        when name = "MANDIRI" then "bg-info"
        when name = "BJB" then "bg-default"
        when name = "RBR" then "bg-danger"
        when name = "TEKTAYA" then "bg-default"
        ELSE "bg-info" END as background from mtr_accounts ma order by name asc';
        $result = $this->db->query($query)->result();
        return $result;
    }
}