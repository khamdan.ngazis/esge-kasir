<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="card-title"><b>Setoran</b></h3>
                        </div>
                        <div class="col-md-12" style="padding-left: 20px;padding-right:20px;">
                            <?php if($this->session->flashdata('message') == "success"){ ?>
                            <div class="alert alert-success">
                                <strong>Success!</strong>
                            </div>
                            <?php }
                            if ($this->session->flashdata('message') == "failed"){
                            ?>
                            <div class="alert alert-danger">
                                <strong>Failed</strong>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="card-body">
                            <div class="card card-tabs card-info">
                                <div class="card-header p-0 pt-1">
                                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="custom-tabs-setoran" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Setoran</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Terima Setoran</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-one">
                                        <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                            <table id="table_setoran" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>kode setor</th>
                                                        <th>Setor ke</th>
                                                        <th>Total Setor</th>
                                                        <th>Jam Setor</th>  
                                                        <th>Status</th>                                        
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                
                                                        foreach ($setoran as $key => $value) {
                                                            if($value->status == 'Success'){
                                                                $class = "success";
                                                            }else{
                                                                $class = "warning";
                                                            }
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->id;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><span class="badge badge-<?php echo $class;?>"><?php echo $value->status;?></span></td>
                                                        <td><?php if ($value->status != "Success"){?><button class="btn btn-success btn-sm" dataid="<?php echo $value->id; ?>" uri="<?php echo BASE_URL; ?>setoran/edit_setoran" onclick="showModal(this)" >Edit</button><?php }?> <a href="<?php echo BASE_URL.'setoran/detail/'.$value->id; ?>" class="btn btn-info btn-sm">Detail</a></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>                
                                        </div>
                                        <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                            <table id="table_terima" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>kode setor</th>
                                                        <th>Setoran Dari</th>
                                                        <th>Total Setor</th>
                                                        <th>Jam Setor</th>  
                                                        <th>Status</th>                                        
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                
                                                        foreach ($terima as $key => $value) {
                                                            if($value->status == 'Success'){
                                                                $class = "success";
                                                            }else{
                                                                $class = "warning";
                                                            }
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->id;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><span class="badge badge-<?php echo $class;?>"><?php echo $value->status;?></span></td>
                                                        <td><a href="<?php echo BASE_URL.'setoran/detail/'.$value->id; ?>" class="btn btn-info btn-sm">Detail</a></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>                
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>
<script>
    function showModal(e){
		event.preventDefault();
		mdl = $('#myModal');
		mdl.modal('show');
		uri = $(e).attr('uri');
        dataid = $(e).attr('dataid');
        datatrx = $(e).attr('datatrx');
 		$.post(uri, {ajax: true, id: dataid}, function (data) {
			mdl.find("div[class=modal-content]").html(data);
            onPrepre();
		});
  }
  $( document ).ready(function() {
    setTimeout(function() {
        $('.alert').fadeOut(3000);
        }, 1000
    );
    $('#table_setoran').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                        "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });
    $('#table_terima').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                        "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });
  })
</script>