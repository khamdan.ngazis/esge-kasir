<div class="modal-header bg-danger">
    <h4 class="modal-title">Konfirmasi Delete Data</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>balance/save" method="POST" id="form_add">
<input type="hidden" name="action" value="delete">
<input type="hidden" name="type" value="<?php echo $type; ?>">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<input type="hidden" name="trx_id" value="<?php echo $trx_id; ?>">
<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label >Jumlah</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" readonly value="<?php echo $balance; ?>" class="form-control" id="balance" name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false ">
            </div>
        </div>
            
    </div>
    <div class="modal-footer">
        <button type="submit" id="submit" class="btn btn-danger btn-sm">Delete</button>
    </div>

</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>