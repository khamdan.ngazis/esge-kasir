<div class="modal-header bg-success">
    <h4 class="modal-title">Edit Data</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>balance/save" method="POST" id="form_add">
<input type="hidden" name="action" value="edit">
<input type="hidden" name="type" value="<?php echo $type; ?>">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<input type="hidden" name="trx_id" value="<?php echo $trx_id; ?>">
<div class="modal-body">
    <?php if ($type == "bank"){ ?>
        <div class="form-group">
            <label >Jenis Transaksi</label>
            <select class="form-control" name="trx_type">
                <option value="1">Saldo Masuk</option>
                <option value="-1" <?php if($trx->trx_type == -1){ echo "Selected";} ?>>Saldo Keluar</option>
            </select>
        </div>

    <?php } ?>
    <div class="col-md-12">
        <div class="form-group">
            <label >Jumlah</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" value="<?php echo $balance; ?>" class="form-control" id="balance" name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false ">
            </div>
        </div>
            
    </div>
    <?php if ($type == "bank"){ ?>
        <div class="form-group">
            <label >Keterangan</label>
            <input type="text" class="form-control" value="<?php echo $trx->transaction; ?>"  id="note" name="note" >
        </div>
        <?php } ?>
    <div class="modal-footer">
        <button type="submit" id="submit" class="btn btn-success btn-sm">Save</button>
    </div>

</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>