<?php
class report extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("ReportModel");
        $this->load->model("UserModel");
        isLogin();
    }

    public function index(){
        $date = $this->input->post('date');
        if ($date == ""){
            $date = date('Y-m-d');
        }
        $data['date'] = $date;
        $data['setoran_esge'] = $this->ReportModel->getSetoranEsge($date);
        $data['tarik_tunai'] = $this->ReportModel->getTarikTunai($date);
        $data['titip_transfer'] = $this->ReportModel->getTitipTransfer($date);
        $data['tektaya'] = $this->ReportModel->getTektaya($date);
        $data['penjualan'] = $this->ReportModel->getPenjualan($date);
        $data['jet'] = $this->ReportModel->getJet($date);
        $data['trx_lain'] = $this->ReportModel->getTrxLain($date);
        $data['total_uang'] = $this->ReportModel->getTotalTransaction($date)->total;
        $data['rekap_acount'] = $this->ReportModel->getRekapAccount($date);
        $data['rekap_listrik'] =$this->ReportModel->getTotalTrxAccount($date, 2); 
        $data['total_jet_plus'] = $this->ReportModel->getTotalJet($date, 1); 
        $data['total_lain_plus'] = $this->ReportModel->getTotalTrxLain($date, 1); 
        $data['total_jet_minus'] = $this->ReportModel->getTotalJet($date, -1); 
        $data['total_lain_minus'] = $this->ReportModel->getTrxLainMinus($date); 
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "report";
		$data['content'] = 'report/default';
		$this->load->view('template',$data);
    }

    public function test(){
        
        $data['setoran_esge'] = $this->getSetoranEsge();
        $data['titip_transfer'] = $this->getTitipTransfer();
        $data['tektaya'] = $this->getTektaya();
        $data['penjualan'] = $this->getPenjualan();
        $data['tarik_tunai'] = $this->getTarikTunai();
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "report";
		$data['content'] = 'report/test';
		$this->load->view('template',$data);
    }

    function getTarikTunai(){
        $data = array();

        $a = new stdClass();
        $a->name = 'Joni';
        $a->bank = 'BCA';
        $a->balance = 500000;
        $a->time = '11:20:00';

        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Ultra';
        $a->bank = 'BNI';
        $a->transaction_type = 'Sesama Bank';
        $a->balance = 150000;
        $a->time = '11:20:00';
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Agam';
        $a->bank = 'PERMATA';
        $a->balance = 500000;
        $a->time = '11:20:00';
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Jamal';
        $a->bank = 'BJB';
        $a->transaction_type = 'Bank Lain';
        $a->balance = 1000000;
        $a->time = '11:20:00';
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->bank = 'BCA';
        $a->name = 'Samuel';
        $a->balance = 700000;
        $a->time = '11:20:00';
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        return $data;
    }
   


    function getSetoranEsge(){
        $data = array();
        $a = new stdClass();
        $a->name = 'Doni';
        $a->balance = 51000;
        $a->income = 51000;
        $a->time = '10:11:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total = $a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Samsudin';
        $a->balance = 79000;
        $a->income = 79000;
        $a->time = '11:11:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Pulsa Xl 50rb';
        $a->balance = 50000;
        $a->income = 52000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Ultra';
        $a->balance = 150000;
        $a->income = 150000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Agam';
        $a->balance = 50000;
        $a->income = 50000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Jamal';
        $a->balance = 150000;
        $a->income = 150000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Samuel';
        $a->balance = 70000;
        $a->time = '11:20:00';
        $a->income = 70000;
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        return $data;
    }
    
    function getTektaya(){
        $data = array();
        $a = new stdClass();
        $a->name = '53961616271';
        $a->balance = 51671;
        $a->income = 52000;
        $a->time = '10:11:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total = $a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Doni';
        $a->balance = 79780;
        $a->income = 80000;
        $a->time = '11:11:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = '5396556271';
        $a->balance = 52500;
        $a->income = 52500;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = '5396121872';
        $a->balance = 151780;
        $a->income = 152000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Agam';
        $a->balance = 152500;
        $a->income = 152500;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Jamal';
        $a->balance = 152500;
        $a->income = 152500;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Samuel';
        $a->balance = 75400;
        $a->time = '11:20:00';
        $a->income = 76000;
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        return $data;
    }
    
    function getPenjualan(){
        $data = array();
        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 20;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana XL';
        $a->price = 10000;
        $a->count = 2;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Im3';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Kartu Perdana Simpati';
        $a->price = 10000;
        $a->count = 5;
        $a->time = '10:11:00';
        $a->total = $a->price * $a->count;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);
        return $data;
    }

    function getTitipTransfer(){
        $data = array();
        $a = new stdClass();
        $a->name = 'Doni';
        $a->bank = 'BCA';
        $a->transaction_type = 'Sesama bank';
        $a->balance = 5000000;
        $a->income = 5010000;
        $a->time = '10:11:00';
        $a->service_fee = $a->income - $a->balance;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        $a->total =$a->income;
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Samsudin';
        $a->bank = 'BRI';
        $a->transaction_type = 'Bank Lain';
        $a->balance = 1006500;
        $a->income = 1015000;
        $a->time = '11:11:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Joni';
        $a->bank = 'BCA';
        $a->transaction_type = 'Sesama bank';
        $a->balance = 50000;
        $a->income = 55000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Ultra';
        $a->bank = 'BNI';
        $a->transaction_type = 'Sesama Bank';
        $a->balance = 150000;
        $a->income = 155000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Agam';
        $a->bank = 'PERMATA';
        $a->transaction_type = 'Bank Lain';
        $a->balance = 500000;
        $a->income = 515000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->name = 'Jamal';
        $a->bank = 'BJB';
        $a->transaction_type = 'Bank Lain';
        $a->balance = 156500;
        $a->income = 165000;
        $a->time = '11:20:00';
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        $a = new stdClass();
        $a->bank = 'BCA';
        $a->transaction_type = 'Sesama Bank';
        $a->name = 'Samuel';
        $a->balance = 70000;
        $a->time = '11:20:00';
        $a->income = 75000;
        $a->service_fee = $a->income - $a->balance;
        $a->total =$a->income;
        $a->created = 'Doni';
        $a->updated = 'Doni';
        array_push($data, $a);

        return $data;
    }
    
}