<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="card-title">Histori Saldo</h3>
                        </div>
                        <div class="col-md-12" style="padding-left: 20px;padding-right:20px;">
                            <?php if($this->session->flashdata('message') == "success"){ ?>
                            <div class="alert alert-success">
                                <strong>Success!</strong>
                            </div>
                            <?php }
                            if ($this->session->flashdata('message') == "failed"){
                            ?>
                            <div class="alert alert-danger">
                                <strong>Failed</strong>
                            </div>
                            <?php } ?>
                        </div>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <form action="<?php echo BASE_URL."balance/detail/".$type."/".$id; ?>" method="post" id="filter">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="date" class="form-control" name="date" id="balance_date" value="<?php echo $date; ?>">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-info">
                                <div class="card-header">
                                    <h3 class="card-title">Saldo <span id="balance_type"><?php echo $name; ?><span></h3>
                                </div>
                                <div class="card-body">
                                
                                    <table id="table_balance" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Jam Trx</th>
                                                <th>Nama</th>
                                                <th>Keterangan</th>
                                                <th>Debet</th>
                                                <th>Kredit</th>
                                                <th>Created At</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>           
                                            <?php
                                                $i = 1;
        
                                                foreach ($balance as $key => $value) {
                                            ?> 
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $value->trx_time;?></td>
                                                <td><?php echo $value->name;?></td>
                                                <td><?php echo $value->transaction;?></td>
                                                <td><?php if ($value->debet != ""){ echo "Rp ".number_format($value->debet);}?></td>
                                                <td><?php if ($value->kredit != ""){echo "Rp ".number_format($value->kredit);}?></td>
                                                <td><?php echo $value->created_at;?></td>
                                                <td><?php if($value->trx_id == ""){ ?> <button uri = "<?php echo BASE_URL; ?>balance/edit" datasaldo="<?php echo $value->kredit; ?>" datatrxid="<?php echo $value->id; ?>" dataid="<?php echo $id; ?>" dataname="<?php echo $name;?>" datatype="<?php echo $type; ?>" class="btn btn-success btn-sm" onclick="showModal(this)"> <i class="fa fas fa-edit"></i></button> <button uri = "<?php echo BASE_URL; ?>balance/delete" datasaldo="<?php echo $value->kredit; ?>" datatrxid="<?php echo $value->id; ?>" dataid="<?php echo $id; ?>" dataname="<?php echo $name;?>" datatype="<?php echo $type; ?>" class="btn btn-danger btn-sm" onclick="showModal(this)"> <i class="fa fas fa-trash"></i></button><?php }?></td>
                                            </tr>
                                            <?php
                                                $i ++;
                                                }
                                            ?>
                                        </tbody>
                                    </table><br>
                                    <button uri = "<?php echo BASE_URL; ?>balance/add" dataid="<?php echo $id; ?>" dataname="<?php echo $name;?>" datatype="<?php echo $type; ?>" class="btn btn-info btn-sm" onclick="showModal(this)"> <i class="nav-icon fa fa-cart-plus"></i> Tambah Saldo</button>
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<script>
    function showModal(e){
		event.preventDefault();
		mdl = $('#myModal');
		mdl.modal('show');
		uri = $(e).attr('uri');
        dataid = $(e).attr('dataid');
        datatrxid = $(e).attr('datatrxid');
        datatype = $(e).attr('datatype');
        dataname = $(e).attr('dataname');
        datasaldo = $(e).attr('datasaldo');
 		$.post(uri, {ajax: true, id: dataid, type: datatype, trx_id: datatrxid, name: dataname, saldo: datasaldo}, function (data) {
			mdl.find("div[class=modal-content]").html(data);
            onPrepre();
		});
  }
  
     $( document ).ready(function() {
        setTimeout(function() {
        $('.alert').fadeOut(3000);
        }, 1000
    );
        $('#table_balance').DataTable({
            "paging": true,
            "lengthChange": false,
            "pageLength":5,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "oLanguage": {
                                "sInfo": "<b>Saldo Saat ini : Rp <?php echo number_format($saldo);?></b>",
                                "sInfoFiltered": "",
                                "sInfoEmpty":"<b>Saldo Saat ini  : Rp <?php echo number_format($saldo);?></b>",
                    
                               "oPaginate": {
                                "sPrevious": "<i class='fa fa-angle-left'></i>",
                                "sNext":"<i class='fa fa-angle-right'></i>"
                            }
                    }
            });
        $('#balance_name').change(function() {
            $('#balance_type').html($('#balance_name').val())
            $('#form_balance_name').html($('#balance_name').val())
        })
     });
     $( "#balance_date" ).change(function() {
        $('#filter').submit();
    })
</script>