<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SetoranModel extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
    }

    function getSetoran($user_id){
        $query = 'select a.id, a.created_at as time , b.name , sum(d.amount * d.trx_type) as total ,
        CASE when a.status = 10 then "New Setoran" ELSE "Success" END as status
        from trx_setoran a
        left join mtr_users b on a.`to` = b.id 
        left join trx_setoran_details c on a.id = c.setoran_id 
        left join trx_transaction d on c.trx_id = d.id 
        where date(a.created_at) = curdate() and a.`from` = '.$user_id.'
        group by a.id ';
        $result = $this->db->query($query)->result();
        return $result;
    }
    function getTerima($user_id){
        $query = 'select a.id, a.created_at as time , b.name , sum(d.amount * d.trx_type) as total ,
        CASE when a.status = 10 then "New Setoran" ELSE "Success" END as status
        from trx_setoran a
        left join mtr_users b on a.`from` = b.id 
        left join trx_setoran_details c on a.id = c.setoran_id 
        left join trx_transaction d on c.trx_id = d.id 
        where date(a.created_at) = curdate() and a.`to` = '.$user_id.'
        group by a.id ';
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getSetoranDetail($setoran_id){
        $query = 'select a.to, a.from, a.id, a.created_at as time , b.name , sum(d.amount) as total ,
        CASE when a.status = 10 then "New Setoran" ELSE "Success" END as status
        from trx_setoran a
        left join mtr_users b on a.`to` = b.id 
        left join trx_setoran_details c on a.id = c.setoran_id 
        left join trx_transaction d on c.trx_id = d.id 
        where a.`id` = '.$setoran_id.'
        group by a.id ';
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getSetoranEsge($setor_id){
        $query = "select a.trx_id , a.trx_account_id , b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        where date(c.create_at) = curdate() and b.account_id = 1 and 
        c.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTektaya($setor_id){
        $query = "select d.name as account_name, a.trx_id , a.trx_account_id , b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_accounts d on b.account_id = d.id
        where date(c.create_at) = curdate() and d.account_type = 2 and 
        c.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTitipTransfer($setor_id){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (c.amount - b.amount )as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = curdate() and b.trx_type = -1 and 
        c.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTarikTunai($setor_id){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (b.amount - c.amount)as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = curdate() and b.trx_type = 1 and 
        c.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getPenjualan($setor_id){
        $query = "select a.trx_id , a.trx_sales_id , b.name , b.price , b.qty as count , b.total , time(b.create_at) as `time` , 
        b.created_by as created , b.updated_by as updated 
        from  trx_transaction_sales a 
        left join trx_sales b on b.id = a.trx_sales_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = curdate() and 
        c.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getJet($setor_id){
        $query = "select a.trx_id , a.trx_courier_id , b.name , (b.trx_type * b.amount) as balance ,
        time(b.created_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_courier a 
        left join trx_courier b on b.id = a.trx_courier_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = curdate() and 
        c.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTrxLain($setor_id){
        $query = "select a.id, a.trx_id , a.note , b.name , (b.trx_type * b.amount) as balance ,
        time(b.create_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_lain a 
        left join trx_transaction b on b.id = a.trx_id
        where date(b.create_at) = curdate() and 
        b.id in (select trx_id from trx_setoran_details tsd where setoran_id = ".$setor_id.")";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function edit_setoran($id,$to_id){
        $query = "update trx_setoran set `to` = ".$to_id." where id = ".$id;
        $result = $this->db->query($query);
        return $result;
     }
     function terima_setoran($id){
        $query = "call terima_setoran(".$id.")";
        $result = $this->db->query($query);
        return $result;
     }
}