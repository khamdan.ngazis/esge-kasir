<?php
class UserModel extends CI_Model {

    public function __construct()
	{
		parent::__construct();
		
	}
	
	public function getUserLogin($username){
		$this->db->select('a.*');
		$this->db->from('mtr_users a');
		$this->db->where('lower(a.username) = "' . strtolower($username) . '"');
		return $this->db->get()->row();
	}

	public function getAllUser(){
		$this->db->select('a.*');
		$this->db->from('mtr_users a');
		return $this->db->get()->result();
	}

	public function getUser($id){
		$this->db->select('a.id,a.username , a.name , a.role');
		$this->db->from('mtr_users a');
		$this->db->where('a.id = "' . $id . '"');
		return $this->db->get()->row();
	}

	public function getUserbyRole($role){
		$this->db->select('a.id,a.username , a.name , a.role');
		$this->db->from('mtr_users a');
		$this->db->where('a.role', $role);
		return $this->db->get()->result();
	}
	
}
?>