<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReportModel extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
    }

    function getTotalTransaction($date){
        $query ="select sum(amount * trx_type) as total from trx_transaction tt where date(create_at) = date('".$date."')";
        $result = $this->db->query($query)->row();
        return $result;
    }
    function getSetoranEsge($date){
        $query = "select a.trx_id , a.trx_account_id , b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        where date(c.create_at) = date('".$date."') and b.account_id = 1";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTektaya($date){
        $query = "select d.name as account_name, a.trx_id , a.trx_account_id , b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_accounts d on b.account_id = d.id
        where date(c.create_at) = date('".$date."') and d.account_type = 2 ";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTotalTrxAccount($date, $trx_type){
        $query ="select ma.id , ma.name ,
        (select sum(c.amount )as total
                from trx_transaction_account a
                left join trx_account b on a.trx_account_id = b.id 
                left join trx_transaction c on a.trx_id = c.id 
                where date(c.create_at) = date('".$date."') and b.account_id = ma.id ) as income,
        (select sum(b.amount )as total
                from trx_transaction_account a
                left join trx_account b on a.trx_account_id = b.id 
                left join trx_transaction c on a.trx_id = c.id 
                where date(c.create_at) = date('".$date."') and b.account_id = ma.id ) as balance
        from mtr_accounts ma where ma.account_type = ".$trx_type;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getRekapAccount($date){
        $query = "select 
        (select name from mtr_accounts where id = ta.account_id ) as name,
        ((select balance from mtr_accounts where id = account_id) + 
        ifnull((select sum(amount) from trx_account tb where date(tb.created_at) = date('".$date."')  
        and tb.account_id = ta.account_id and trx_type = -1),0) - 
        ifnull((select sum(amount) 
        from trx_account tb where date(tb.created_at) = date('".$date."') 
        and tb.account_id = ta.account_id and trx_type = 1), 0) ) as saldo_awal,
        ifnull((select sum(amount) from trx_account tb where date(tb.created_at) = date('".$date."') 
        and tb.account_id = ta.account_id and trx_type = -1),0) as saldo_keluar, 
        ifnull((select sum(amount) from trx_account tb where date(tb.created_at) = date('".$date."') 
        and tb.account_id = ta.account_id and trx_type = 1),0) as tambah_saldo,
        (select balance from mtr_accounts where id = account_id)as saldo_akhir
        from trx_account ta where date(created_at) = date('".$date."') group by account_id ";
        $result = $this->db->query($query)->result();
        return $result;
    }
    function getTitipTransfer($date){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (c.amount - b.amount )as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = date('".$date."') and b.trx_type = -1 ";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTarikTunai($date){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (b.amount - c.amount)as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = date('".$date."') and b.trx_type = 1";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getPenjualan($date){
        $query = "select a.trx_id , a.trx_sales_id , b.name , b.price , b.qty as count , b.total , time(b.create_at) as `time` , 
        b.created_by as created , b.updated_by as updated 
        from  trx_transaction_sales a 
        left join trx_sales b on b.id = a.trx_sales_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = date('".$date."')";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getJet($date){
        $query = "select a.trx_id , a.trx_courier_id , b.name , (b.trx_type * b.amount) as balance ,
        time(b.created_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_courier a 
        left join trx_courier b on b.id = a.trx_courier_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = date('".$date."')";
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function getTotalJet($date, $trx_type){
        $query = " select sum(c.amount ) total
        from  trx_transaction_courier a 
        left join trx_courier b on b.id = a.trx_courier_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = date('".$date."') and c.trx_type = ".$trx_type;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTotalTrxLain($date,$trx_type){
        $query = "select sum(b.amount ) total
        from  trx_transaction_lain a 
        left join trx_transaction b on b.id = a.trx_id
        where date(b.create_at) = date('".$date."') and b.trx_type = ".$trx_type;;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTrxLain($date){
        $query = "select a.id, a.trx_id , a.note , b.name , (b.trx_type * b.amount) as balance ,
        time(b.create_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_lain a 
        left join trx_transaction b on b.id = a.trx_id
        where date(b.create_at) = date('".$date."')";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTrxLainMinus($date){
        $query = "select a.id, a.trx_id , a.note , b.name ,b.amount as balance ,
        time(b.create_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_lain a 
        left join trx_transaction b on b.id = a.trx_id
        where date(b.create_at) = date('".$date."') and b.trx_type = -1";
        $result = $this->db->query($query)->result();
        return $result;
    }


}