<form action="<?php echo BASE_URL; ?>transaction/save_penjualan" method="POST" id="form_add">
<input type="hidden" name="action" value="add">
<div class="modal-header bg-info">
    <h4 class="modal-title">Add Transaksi</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>

<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label >Nama Barang</label>
            <input type="text" class="form-control" placeholder="Masukan Nama Barang" name="name" required>
        </div>
        <div class="form-group">
            <label >Harga Satuan</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" class="form-control" id="price" name="price" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false ">
            </div>
        </div>

        <div class="form-group">
            <label >Jumlah</label>
            <input type="number" class="form-control" id='count' name="qty" required>
        </div>
        <div class="form-group">
            <label >Total</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" readonly class="form-control" id='total' data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false " name="total" required>
            </div>
        </div>
    </div>      
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-info btn-sm">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#price").inputmask();
        $("#total").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });
    $( "#price" ).change(function() {
        var price = $("#price").val();
        var count = $("#count").val();
        var target = 'total';
        count_total(price , count , target);
    })

    $( "#count" ).change(function() {
        var price = $("#price").val();
        var count = $("#count").val();
        var target = 'total';
        count_total(price , count , target);
    })

</script>

