<div class="modal-header bg-info">
    <h4 class="modal-title">Transfer Uang ke User Lain</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>transaction/save_trx_transfer_uang" method="POST" id="form_add">
<input type="hidden" name="action" value="add">
<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label >Kirim Ke</label>
                    <select class="form-control" name="transfer_to" required>
                        <option value="">Pilih</option>
                        <?php foreach ($users as $key => $value) {
                            if ($value->name != $this->session->userdata("SesiName")){
                        ?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
                        <?php
                            }
                        }?>
                        
                    </select>
                </div>
                <div class="form-group">
                    <label >Jumlah</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" class="form-control" id='balance' name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false" required>
                    </div>
                </div>
                <div class="form-group">
                    <label >Total Uang</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" readonly value="<?php echo $total; ?>" class="form-control" id='total_income' name="total_income" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false">
                    </div>
                </div>
            </div>
            <span style="color:red;display:none" id="notif">Uang tidak cukup</span>
            <hr>
        </div>
    </div>
</div>
<div class="modal-footer">
    
    <button type="submit" id="submit" class="btn btn-info btn-sm" style="width: 100px;">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
        $("#total_income").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
        var jumlah = $("#balance").val();
        jumlah = jumlah.replace(/./gi,"");
        jumlah = jumlah.replace(/,/gi,"");
        var income = $("#total_income").val();
        income = income.replace(/./gi);
        income = income.replace(/,/gi,"");
        if (jumlah > income ){
            alert(income);
            $('#submit').html('Save');
            $('#submit').removeAttr('disabled');
            $('#notif').show(100);
            return false;
        }
        
    });

</script>
