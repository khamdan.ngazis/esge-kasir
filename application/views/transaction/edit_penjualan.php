<form action="<?php echo BASE_URL; ?>transaction/save_penjualan" method="POST" id="form_add">
<input type="hidden" name="account_id" value="<?php echo $trx->trx_sales_id; ?>">
<input type="hidden" name="trx_id" value="<?php echo $trx->trx_id; ?>">
<input type="hidden" name="action" value="edit">
<div class="modal-header bg-success">
    <h4 class="modal-title">Edit Transaksi</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>

<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <label >Nama Barang</label>
            <input type="text" class="form-control" value="<?php echo $trx->name; ?>" placeholder="Masukan Nama Barang" name="name" required>
        </div>
        <div class="form-group">
            <label >Harga Satuan</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" value="<?php echo $trx->price; ?>" class="form-control" id="price" name="price" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false ">
            </div>
        </div>

        <div class="form-group">
            <label >Jumlah</label>
            <input type="number" value="<?php echo $trx->count; ?>" class="form-control" id='count' name="qty" required>
        </div>
        <div class="form-group">
            <label >Total</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text"  value="<?php echo $trx->total; ?>"  readonly class="form-control" id='total' data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false " name="total" required>
            </div>
        </div>
    </div>      
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-success btn-sm">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#price").inputmask();
        $("#total").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });
    $( "#price" ).change(function() {
        var price = $("#price").val();
        var count = $("#count").val();
        var target = 'total';
        count_total(price , count , target);
    })

    $( "#count" ).change(function() {
        var price = $("#price").val();
        var count = $("#count").val();
        var target = 'total';
        count_total(price , count , target);
    })

</script>

