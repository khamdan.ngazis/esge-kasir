<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
ini_set('display_errors', 1);
if ( ! function_exists('send_mail'))
{
    function send_mail($data)
    {
        $CI =& get_instance();
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => $CI->config->item('smtp_host'),
            'smtp_user' => $CI->config->item('smtp_user'),  // Email gmail
            'smtp_pass'   => $CI->config->item('smtp_pass'),  // Password gmail
            'smtp_crypto' => $CI->config->item('smtp_crypto'),
            'smtp_port'   => $CI->config->item('smtp_port'),
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        // Load library email dan konfigurasinya

        $CI->load->library('email', $config);
        $CI->email->from($CI->config->item('smtp_user'), 'winery.id');

        // Email penerima
        $CI->email->to($data['email_penerima']); // Ganti dengan email tujuan

        // Lampiran email, isi dengan url/path file
        //$CI->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

        // Subject email
        $CI->email->subject($data['subject']);

        // Isi email
        $CI->email->message($data['message']);

        // Tampilkan pesan sukses atau error
        if ($CI->email->send()) {
            return 1;
        } else {
            return 0;
        }
    }   
}