<div class="modal-header bg-success">
    <h4 class="modal-title">Edit Transaction RBR</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>transaction/save_esge" method="POST" id="form_add">
<input type="hidden" name="account_id" value="<?php echo $trx->trx_account_id; ?>">
<input type="hidden" name="trx_id" value="<?php echo $trx->trx_id; ?>">
<input type="hidden" name="action" value="edit">
<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Nama</label>
                    <input type="text" class="form-control" value="<?php echo $trx->name;;?>" placeholder="Masukan Nama" name="name" required>
                </div>
                <div class="form-group">
                    <label >Waktu Transaksi</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-clock"></i></span>
                        </div>
                        <input type="time" class="form-control" value="<?php echo $trx->time;?>" placeholder="Contoh : 10:00:00" name="trx_time">
                    </div>
                </div>
                <div class="form-group">
                    <label >Jasa</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" value="<?php echo $trx->service_fee;?>" class="form-control" id="service" name="service" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false " readonly>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Saldo Terpotong</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" value="<?php echo $trx->balance;?>" class="form-control" id='balance' name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false" required>
                    </div>
                </div>
                <div class="form-group">
                    <label >Uang Masuk</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" value="<?php echo $trx->income;?>" class="form-control" id='income' data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false " name="income" required>
                    </div>
                </div>
            
            </div>
            <hr>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-success btn-sm" style="width: 100px;">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
        $("#income").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });
    $( "#income" ).change(function() {
        var balance = $("#balance").val();
        var income = $("#income").val();
        var target = 'service';
        count_service(balance , income , target);
    })

    $( "#balance" ).change(function() {
        var balance = $("#balance").val();
        var income = $("#income").val();
        var target = 'service';
        count_service(balance , income , target);
    })

</script>
