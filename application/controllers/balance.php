<?php
class balance extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("DashboardModel");
        $this->load->model("BalanceModel");
        isLogin();
    }

    public function index(){
        $data['balance'] = $this->DashboardModel->getAccountBalance();
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "balance";
		$data['content'] = 'balance/default';
		$this->load->view('template',$data);
    }

    public function detail(){
        $date = $this->input->post('date');
        if ($date == ""){
            $date = date('Y-m-d');
        }
        $type = $this->uri->segment(3);
        if ($type == ""){
            redirect(BASE_URL.'balance');
        }
        $id = $this->uri->segment(4);
        if ($id == ""){
            redirect(BASE_URL.'balance');
        }
        $data['date'] =  $date;
        if ($type == "bank"){
            $data['balance'] = $this->BalanceModel->getHistoryBank($id,$date);
            $data['name'] =  $this->BalanceModel->getBankDetail($id)->name;
            $data['saldo'] = $this->BalanceModel->getBankDetail($id)->balance;
        }else{
            $data['balance'] = $this->BalanceModel->getHistoryAccount($id,$date);
            $data['name'] =  $this->BalanceModel->getAccountDetail($id)->name;
            $data['saldo'] = $this->BalanceModel->getAccountDetail($id)->balance;
        }
        $data['id'] = $id;
        $data['type'] = $type;
        
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "balance";
		$data['content'] = 'balance/history';
		$this->load->view('template',$data);
    }

    public function add(){
        $data['id'] = $this->input->post('id');
        $data['type'] = $this->input->post('type');
        $data['name'] = $this->input->post('name');
        $this->load->view('balance/add',$data);
    }

    public function edit(){
        $data['id'] = $this->input->post('id');
        $data['type'] = $this->input->post('type');
        $data['name'] = $this->input->post('name');
        $data['trx_id'] = $this->input->post('trx_id');
        $data['balance'] = $this->input->post('saldo');
        if ($data['type'] == "bank"){
            $data['trx'] = $this->BalanceModel->getTrxbankDetail($data['trx_id']);
        }
        $this->load->view('balance/edit',$data);
    }

    public function delete(){
        $data['id'] = $this->input->post('id');
        $data['type'] = $this->input->post('type');
        $data['name'] = $this->input->post('name');
        $data['trx_id'] = $this->input->post('trx_id');
        $data['balance'] = $this->input->post('saldo');
        $this->load->view('balance/delete',$data);
    }

    public function save(){
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $action = $this->input->post('action');
        $balance_type = $this->input->post('type');
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $trx_type = "Tambah Saldo";
        $transaction_name =  "Tambah Saldo";
        $trx_time = date('H:i:s');
        if ($balance_type == "bank"){
            $trx_type =  $this->input->post('note');
            $jenis_trx =  $this->input->post('trx_type');
            if ( $jenis_trx == 1){
                $transaction_name = "Saldo Masuk";
            }else{
                $transaction_name = "Saldo Keluar";
            }
            if ( $action == "add"){
                $save = $this->BalanceModel->add_transaction_bank($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $balance, $trx_type, $jenis_trx);
            }
            if ( $action == "edit"){
                $save = $this->BalanceModel->edit_transaction_bank($trx_id ,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $balance, $trx_type, $jenis_trx);
            }
            if ( $action == "delete"){
                $save = $this->BalanceModel->delete_transaction_bank($trx_id);
            }
        }else{
            if ( $action == "add"){
                $save = $this->BalanceModel->add_transaction_account($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $balance);
            }
            if ( $action == "edit"){
                $save = $this->BalanceModel->edit_transaction_account($trx_id , $user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $balance);
            }
            if ( $action == "delete"){
                $save = $this->BalanceModel->delete_transaction_account($trx_id);
            }
        }
        $this->session->set_flashdata('tab', 'tarik');
        $this->session->set_flashdata('message', 'success');
        $link = "balance/detail/".$balance_type."/".$account_id;
        redirect(BASE_URL.$link);
    }
    
}