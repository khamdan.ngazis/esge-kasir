<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Esge Kasir</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- IonIcons 
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  -->
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro 
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  -->

  <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- AdminLTE -->
    <script src="<?php echo BASE_URL; ?>assets/dist/js/adminlte.js"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/chart.js/Chart.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/dist/js/demo.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/dist/js/pages/dashboard3.js"></script>
    <!-- bs-custom-file-input -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <!-- Summernote -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo BASE_URL; ?>assets/plugins/select2/js/select2.full.min.js"></script>
    
    <script src="<?php echo BASE_URL; ?>assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini accent-info">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-info">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
          <span class="hidden-xs"><?php echo $this->session->userdata("SesiName"); ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="width:200px;" >
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media" >
              <img src="<?php echo BASE_URL; ?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                <?php echo $this->session->userdata("SesiName"); ?>
                </h3>
                <p class="text-sm"><?php echo $this->session->userdata("SesiRole"); ?></p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo BASE_URL; ?>login/logout" class="dropdown-item dropdown-footer">Logout</a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-info">
    <!-- Brand Logo -->
    <a href="#" class="brand-link navbar-info" >
      
      <img src="<?php echo BASE_URL; ?>assets/dist/img/icon.png" alt="AdminLTE Logo"  class="brand-image img-circle elevation-3"
           style="width:30px;background-color:#fff">
      
      <span class="brand-text font-weight-dark" style="color:#FFF" >ESGE KASIR</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-compact nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo BASE_URL; ?>dashboard" class="nav-link <?php if($link == 'dashboard'){ echo "active";} ?>" >
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo BASE_URL; ?>transaction" class="nav-link <?php if($link == 'transaksi'){ echo "active";} ?>" >
              <i class="nav-icon fa fa-cart-plus"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo BASE_URL; ?>setoran" class="nav-link <?php if($link == 'setoran'){ echo "active ";} ?>">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Setoran
              </p>
            </a>
          </li>
          <?php if ($this->session->userdata("SesiRole") == "Kasir" || $this->session->userdata("SesiRole") == "Owner"){
          ?>
         <li class="nav-item">
            <a href="<?php echo BASE_URL; ?>balance" class="nav-link <?php if($link == 'balance'){ echo "active";} ?>" >
              <i class="nav-icon fas fa-landmark"></i>
              <p>
                Saldo
              </p>
            </a>
          </li>
          <?php }?>
          <?php if ($this->session->userdata("SesiRole") == "Owner"){
          ?>
          <li class="nav-item">
            <a href="<?php echo BASE_URL; ?>report" class="nav-link <?php if($link == 'report'){ echo "active";} ?>" >
              <i class="nav-icon fas fa-file-alt"></i>
              <p>
                Report
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo BASE_URL; ?>user" class="nav-link <?php if($link == 'user'){ echo "active";} ?>" >
              <i class="nav-icon fas fa-user-tie"></i>
              <p>
                Manage User
              </p>
            </a>
          </li>
          <?php } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

