
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12" >
                    <div class="card"  >
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Edit User</h3>
                            </div>
                        </div>
                        <div class="card-body">
                        <form action="<?php echo BASE_URL.'user/save' ?>" method="post">
                            <input type="hidden" name="Id" value="<?php echo $user->id; ?>">
                            <div class="form-group">
                                <label for="Username">Username</label>
                                <input type="text"  value="<?php echo $user->username; ?>" name="Username" class="form-control" id="Username" placeholder="Enter Username" required>
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" name="Password" class="form-control" id="Password" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <label for="Name">Name</label>
                                <input type="text" value="<?php echo $user->name; ?>" name="Name" class="form-control" id="Name" placeholder="Enter Name" required>
                            </div>
                            <div class="form-group">
                                <label for="Role">Role</label>
                                <select class="form-control" name="Role" required>
                                    <option value="Owner" <?php if($user->role == 'Owner'){ echo 'selected';} ?>>Owner</option>
                                    <option value="Kasir" <?php if($user->role == 'Kasir'){ echo 'selected';} ?>>Kasir</option>
                                    <option value="Customer Service" <?php if($user->role == 'Customer Service'){ echo 'selected';} ?>>Customer Service</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-info">Submit</button> 
                        </form>
                        </div>
                    </div>
                </div>

            </div>
        <!-- /.row -->
        </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->