<div class="modal-header bg-success">
    <h4 class="modal-title">Edit Transaction</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>transaction/save_jet" method="POST" id="form_add">
<input type="hidden" name="action" value="edit">
<input type="hidden" name="trx_type" value="<?php echo $trx->trx_type; ?>">
<input type="hidden" name="account_id" value="<?php echo $trx->trx_courier_id; ?>">
<input type="hidden" name="trx_id" value="<?php echo $trx->trx_id; ?>">
<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php if ($trx->trx_type == -1){ ?>
                    <label >Setoran JET</label>
                    <?php } else { ?>
                        <label >Nama / Nomor Resi</label>
                    <?php }?> 
                    <input type="text" <?php if ($trx->trx_type == -1){echo "readonly";} ?> value="<?php echo $trx->name; ?>" class="form-control" placeholder="Masukan Nama" name="name" required>
                </div>
                <div class="form-group">
                    <label >Biaya</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" value="<?php echo $trx->balance; ?>" class="form-control" id='balance' name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false" required>
                    </div>
                </div>
            
            </div>
            <hr>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-success btn-sm" style="width: 100px;">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>
