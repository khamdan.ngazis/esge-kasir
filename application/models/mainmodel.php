<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MainModel extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
	}
//-------------------------------------------Aktif record---------------------------------------------//
	/*keterangan variabel :
	$table : nama tabel
	$field : nama field
	$id    : isi dari sebuah field 
	$sb    : sort by (asc atau desc)
	$gb    : group by (nama fieldnya)
	$com   : bentuk array ('field'=>'key') untuk pencarian
	$set   : bentuk array ('field'=>'isi') untuk input,update
	*/
	//mencari semua record berdasarkan tabel aja
	public function msr($table,$field,$sb){
		$this->db->order_by($field,$sb);
		return $this->db->get($table);
	}
	
	public function msrgp($table,$field,$sb,$gb){
		$this->db->group_by($gb); 
		$this->db->order_by($field,$sb);
		return $this->db->get($table);
	}
	
	public function msrpag($table,$field,$sb,$limit,$offset){
		$this->db->order_by($field,$sb);
		$this->db->limit($limit, $offset);
		return $this->db->get($table);
	}
	
	//mencari semua record berdasarkan kondisi 
	public function msrwhere($table,$com,$field,$sb){
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com);
	}
	
	public function msrwherelimit($table,$com,$field,$sb,$lim){
		$this->db->order_by($field,$sb);
		$this->db->limit($lim);
		return $this->db->get_where($table, $com);
	}
	
	public function msrwherelimitspec($table,$com,$field,$sb,$lim){
		$this->db->order_by($field,$sb);
		$this->db->limit($lim);
		return $this->db->get_where($table, $com);
	}
	
	public function msrwhere_select($table,$com,$field,$sb,$select){
		$this->db->select($select);
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com);
	}
	
	public function msr_select($table,$field,$sb,$select){
		$this->db->select($select);
		$this->db->order_by($field,$sb);
		$query = $this->db->get($table);
		return $query;
	}
	
	public function msrwhere_select_group($table,$field,$sb,$select,$field2, $names){
		$this->db->select($select);
		$this->db->group_by($select); 
		$this->db->order_by($field,$sb);
		$this->db->where_in($field2, $names);
		return $this->db->get($table);
	}
	
	public function msrwhereJoin2Table($table1,$table2,$com,$com2,$id,$field,$sb,$select){
		$this->db->select($select);
		$this->db->from($table1);
		$this->db->order_by($field,$sb);
		$this->db->join($table2, $com);
		$this->db->where($com2, $id);
		return $this->db->get();
	}
	
	public function msrwhereJoin3Table($table1,$table2,$table3,$com1,$com2,$field,$field2,$sb,$select,$com3,$id){
		$this->db->select($select);
		$this->db->from($table1);
		$this->db->join($table2, $com1);
		$this->db->join($table3, $com2);
		$this->db->order_by($field,$sb);
		$this->db->order_by($field2,$sb);
		$this->db->where($com3, $id);
		return $this->db->get();
	}
	
	public function msrwheregp($select,$table,$com,$field,$sb){
		$this->db->select($select);
		$this->db->group_by($select);
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com);
	}
	
	public function msrwherepag($table,$com,$field,$sb,$limit,$offset){
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com, $limit, $offset);
	}
	
	public function msrwherepaggr($table,$com,$limit,$offset,$gb){
		$this->db->group_by($gb);
		return $this->db->get_where($table, $com, $limit, $offset);
	}
		
	//save record berdasarkan tabel
	function save($table, $set){
		$this->db->insert($table, $set);
		return $this->db->insert_id();
		//$this->db_debug = $oldv;
	}
	
	//update record bebrdasarkan tabel,field dan key
	function edit($table, $set,$field, $id){
		$this->db->where($field, $id);
		return $this->db->update($table, $set);
	}
	
	//delete record bebrdasarkan tabel,field dan key
	function delete($table,$field,$id){
		return $this->db->delete($table, array($field => $id));
	}
	
	//mencari semua record dengan kondisi like
	function msrseek($keyword,$table,$field){
	        $this->db->select('*')->from($table);
	        $this->db->like($field,strtoupper($keyword),'after');
	        $query = $this->db->get();
			return $query;
	}
	
	function msrseek2($keyword,$table,$field,$com){
			$this->db->select('*')->from($table);
			$this->db->where($com);
	        $this->db->like($field,$keyword,'after');
	        $query = $this->db->get();
			return $query;
	}
			
	function msrquery($sql){
		$query = $this->db->query($sql);
		return $query;
	}
	
	function execquery($sql){
		$this->db->query($sql);
	}
	
	function seekLookup($keyword,$table,$field,$short,$data,$name){
	        $this->db->select('*');
	        $this->db->like($name,$keyword);
			$this->db->order_by($field,$short);
			$this->db->limit(10);
	        return $this->db->get_where($table,$data);
	    }
		
	function seekLookup2($keyword,$table,$field,$short,$data,$name,$field2,$names){
	        $this->db->select('*');
	        $this->db->like($name,$keyword);
			$this->db->order_by($field,$short);
			$this->db->limit(10);
			$this->db->where_in($field2, $names);
	        return $this->db->get_where($table,$data);
	    }
	
	function seek($keyword,$table,$field,$data){
	        $this->db->like($field,$keyword,'after');
	        $query = $this->db->get_where($table,$data);
	 
	        return $query;
	    }
	
	function seek2($keyword,$table,$field,$short,$data,$num,$offset){
	        $this->db->like($field,$keyword,'after');
			$this->db->order_by($field,$short);
			$this->db->limit($num,$offset);
	        $query = $this->db->get_where($table,$data);
	 
	        return $query;
	    }
		
	public function msrNot($table,$field,$sb,$field2, $names){
		$this->db->order_by($field,$sb);
		$this->db->where_not_in($field2, $names);
		return $this->db->get($table);
	}
	
	public function msrNot2($table,$field,$sb,$field2, $names,$data){
		$this->db->order_by($field,$sb);
		$this->db->where_not_in($field2, $names);
		return $this->db->get_where($table,$data);
	}
	
	public function msrIn($table,$field,$sb,$field2, $names,$data){
		$this->db->order_by($field,$sb);
		$this->db->where_in($field2, $names);
		return $this->db->get_where($table,$data);
	}
	
	function msrIngb($select,$table,$field,$short,$data,$field2,$names){
	        $this->db->select($select);
			$this->db->order_by($field,$short);
			$this->db->group_by($select);
			$this->db->where_in($field2, $names);
	        return $this->db->get_where($table,$data);
	    }
	
	function msrmax($table,$field){
		$this->db->select_max($field,'id');
		return $this->db->get($table);
	}
	
	function msrmax_where($table,$field,$com){
		$this->db->select_max($field,'id');
		return $this->db->get_where($table,$com);
	}
	
	function todate($field){
		$sql = "SELECT TO_DATE ('".$field."', 'MM/DD/YYYY') AS TANGGAL
  				FROM DUAL";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function todate_full($field){
		$sql = "SELECT TO_TIMESTAMP ('".$field."', 'MM/DD/YYYY HH24:mi:ss') AS TANGGAL
  				FROM DUAL";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function todatetime($field){
		$sql = "SELECT TO_TIMESTAMP ('".$field."', 'MM/DD/YYYY HH24:mi') AS TANGGAL
  				FROM DUAL";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function sysdate(){
		$query = $this->db->query('SELECT SYSDATE FROM DUAL')->row();
		$sysdate = $query->SYSDATE;
		return $sysdate;
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */