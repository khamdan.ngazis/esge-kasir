<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="card-title"><b>Report Transaksi</b></h3>
                        </div>
                        <div class="col-md-12" style="padding-left: 20px;padding-right:20px;">
                            <?php if($this->session->flashdata('message') == "success"){ ?>
                            <div class="alert alert-success">
                                <strong>Success!</strong>
                            </div>
                            <?php }
                            if ($this->session->flashdata('message') == "failed"){
                            ?>
                            <div class="alert alert-danger">
                                <strong>Failed</strong>
                            </div>
                            <?php } ?>
                        </div>
                        <?php
                            $tab = $this->session->flashdata('tab');
                            $tab_esge = "";
                            $tab_titip = "";
                            $tab_tektaya = "";
                            $tab_sales = "";
                            $tab_tarik = "";
                            $tab_jet = "";
                            $tab_lain = "";

                            $tab_esge_list = "";
                            $tab_titip_list = "";
                            $tab_tektaya_list = "";
                            $tab_sales_list = "";
                            $tab_tarik_list = "";
                            $tab_jet_list = "";
                            $tab_lain_list = "";
                            switch ($tab) {
                                case 'esge':
                                    $tab_esge = "active";
                                    $tab_esge_list = "show active";
                                    break;
                                case 'titip':
                                    $tab_titip = "active";
                                    $tab_titip_list = "show active";
                                    break;
                                case 'tektaya':
                                    $tab_tektaya = "active";
                                    $tab_tektaya_list = "show active";
                                    break;
                                case 'sales':
                                    $tab_sales = "active";
                                    $tab_sales_list = "show active";
                                    break;
                                case 'tarik':
                                    $tab_tarik = "active";
                                    $tab_tarik_list = "show active";
                                    break;
                                case 'jet':
                                    $tab_jet = "active";
                                    $tab_jet_list = "show active";
                                    break;
                                case 'lain':
                                    $tab_lain = "active";
                                    $tab_lain_list = "show active";
                                    break;
                                default:
                                    $tab_esge = "active";
                                    $tab_esge_list = "show active";
                                    break;
                            }
                            
                        ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <form action="<?php echo BASE_URL."report"; ?>" method="post" id="filter">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="date" class="form-control" name="date" id="balance_date" value="<?php echo $date; ?>">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card card-tabs card-info">
                                <div class="card-header p-0 pt-1">
                                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_esge; ?>" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Setoran RBR</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_titip; ?>" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Titip Transfer</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_tektaya; ?>" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Tektaya</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_sales; ?>" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Penjualan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_tarik; ?>" id="tabs_tarik_tunai-tab" data-toggle="pill" href="#tabs_tarik_tunai" role="tab" aria-controls="tabs_tarik_tunai" aria-selected="false">Tarik Tunai</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_jet; ?>" id="tabs_jet-tab" data-toggle="pill" href="#tabs_jet" role="tab" aria-controls="tabs_jet" aria-selected="false">JET Express</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_lain; ?>" id="tabs_lain-tab" data-toggle="pill" href="#tabs_lain" role="tab" aria-controls="tabs_lain" aria-selected="false">Lain lain</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-one">
                                        <div class="tab-pane fade <?php echo $tab_esge_list; ?>" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                            <table id="table_setoran_esge" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Saldo Terpotong</th>
                                                        <th>Jasa</th>
                                                        <th>Uang Masuk</th> 
                                                        <th>Jam Trx</th> 
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                                
                                                       </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_setoran_esge = 0;
							                            $total_jasa_esge = 0;
                                                        foreach ($setoran_esge as $key => $value) {
                                                            $total_setoran_esge = $total_setoran_esge + $value->total;
                                                    	$total_jasa_esge = $total_jasa_esge + $value->service_fee;
							                        ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp. <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                            </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                                  </div>
                                        <div class="tab-pane fade <?php echo $tab_titip_list; ?>" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                            <table id="table_titip_transfer" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Bank</th>
                                                        <th>Jenis Transaksi</th>
                                                        <th>Saldo Terpotong</th>
                                                        <th>Jasa</th>
                                                        <th>Uang Masuk</th> 
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                           
                                                          </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_titip_transfer = 0;
							                            $total_jasa_tt = 0;
                                                        foreach ($titip_transfer as $key => $value) {
							                            $total_jasa_tt = $total_jasa_tt + $value->service_fee;
                                                        $total_titip_transfer = $total_titip_transfer + $value->total;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td><?php echo $value->bank;?></td>
                                                        <td><?php echo $value->transaction_type;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp. <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                           </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                             </div>
                                        <div class="tab-pane fade <?php echo $tab_tektaya_list; ?>" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                        <table id="table_tektaya" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Account</th>
                                                        <th>Saldo Terpotong</th>
                                                        <th>Jasa</th>
                                                        <th>Uang Masuk</th> 
                                                        <th>Jam Trx</th>     
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                            
                                                         </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_tektaya = 0;
							                            $jasa_tektaya = 0;
                                                        foreach ($tektaya as $key => $value) {
                                                            $total_tektaya = $total_tektaya + $value->total;
 							                                $jasa_tektaya = $jasa_tektaya + $value->service_fee;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td><?php echo $value->account_name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp. <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>                                                       
                                                          </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                             </div> 
                                        <div class="tab-pane fade <?php echo $tab_sales_list; ?>" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                                        <table id="table_penjualan" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga Satuan</th>
                                                        <th>Banyaknya</th>
                                                        <th>Total</th> 
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                              
                                                           </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_penjualan = 0;
                                                        foreach ($penjualan as $key => $value) {
                                                            $total_penjualan = $total_penjualan + $value->total;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->price);?></td>
                                                        <td><?php echo $value->count;?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                            </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                               </div>
                                        <div class="tab-pane fade <?php echo $tab_tarik_list; ?>" id="tabs_tarik_tunai" role="tabpanel" aria-labelledby="tabs_tarik_tunai">
                                            <table id="table_tarik_tunai" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Bank</th>
                                                        <th>Tarik Tunai</th>
                                                        <th>Jasa</th>
                                                        <th>Total Tarik Tunai</th>
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                           
                                                         </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_tarik_tunai = 0;
							                            $jasa_tarik = 0;
                                                        foreach ($tarik_tunai as $key => $value) {
							                            $jasa_tarik = $jasa_tarik + $value->service_fee;
                                                        $total_tarik_tunai = $total_tarik_tunai + $value->income;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td><?php echo $value->bank;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->income);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                              </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                                </div>
                                        <div class="tab-pane fade <?php echo $tab_jet_list; ?>" id="tabs_jet" role="tabpanel" aria-labelledby="tabs_jet">
                                            <table id="table_jet" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama / No Resi</th>
                                                        <th>Jasa</th>
                                                        <th>Total</th>
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                            
                                                            </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_jet = 0;
                                                        foreach ($jet as $key => $value) {
                                                            $total_jet = $total_jet + $value->balance;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                             </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                                </div>
                                        <div class="tab-pane fade <?php echo $tab_lain_list; ?>" id="tabs_lain" role="tabpanel" aria-labelledby="tabs_lain">
                                            <table id="table_lain" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Total</th>
                                                        <th>Jam Trx</th>  
                                                        <th>Keterangan</th> 
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                           
                                                             </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_lain = 0;
                                                        foreach ($trx_lain as $key => $value) {
                                                            $total_lain = $total_lain + $value->balance;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->note;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                          </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            </div>
                                        
                                    </div>
                                </div>
                            <!-- /.card -->
                            </div>
                                                        
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            Rekap Saldo
                                        </div>
                                        <div class="card-body">
                                            <table id="table_rekap_saldo" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Saldo Awal</th>
                                                        <th>Tambah Saldo</th>
                                                        <th>Saldo Keluar</th>   
                                                        <th>Saldo Akhir</th>                                        
                                                    </tr>
                                                </thead>
                                                <tbody>    
                                                <?php
                                                        foreach ($rekap_acount as $key => $value) {
                                                         ?> 
                                                    <tr>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->saldo_awal);?></td>
                                                        <td>Rp <?php echo number_format($value->tambah_saldo);?></td>
                                                        <td>Rp <?php echo number_format($value->saldo_keluar);?></td>
                                                        <td>Rp <?php echo number_format($value->saldo_akhir);?></td>
                                                          </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                <div class="card card-info">
                                        <div class="card-header">
                                            Rekap Jasa
                                        </div>
                                        <div class="card-body">
                                            <table id="table_rekap_saldo" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Transaksi</th>
                                                        <th>Jumlah</th>                                     
                                                    </tr>
                                                </thead>
                                                <tbody>    
                                                    <tr>
                                                        <td>Total Jasa</td>
                                                        <td>Rp <?php echo number_format($total_jasa_esge + $total_jasa_tt + $jasa_tektaya);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jasa Tarik Tunai</td>
                                                        <td>Rp <?php echo number_format($jasa_tarik);?></td>
                                                    </tr>
                                                </tbody>
                                                <thead class="bg-secondary">
                                                    <tr>
                                                        <th>Total Semua Jasa</th>
                                                        <th>Rp <?php echo number_format($total_jasa_esge + $total_jasa_tt + $jasa_tektaya + $jasa_tarik);?></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Jasa Dipotong 2,5 %</th>
                                                        <th>Rp <?php echo number_format(($total_jasa_esge + $total_jasa_tt + $jasa_tektaya + $jasa_tarik) - (($total_jasa_esge + $total_jasa_tt + $jasa_tektaya + $jasa_tarik) * (2.5 / 100)));?></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            Rekap Uang Masuk
                                        </div>
                                        <div class="card-body">
                                            <table id="table_rekap_saldo" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Transaksi</th>
                                                        <th>Uang Masuk</th>
                                                        <th>Jasa</th>
                                                        <th>Total Uang Masuk</th>                                         
                                                    </tr>
                                                </thead>
                                                <tbody>    
                                                    <tr>
                                                        <td>Setoran RBR</td>
                                                        <td>Rp <?php echo number_format($total_setoran_esge - $total_jasa_esge);?></td>
                                                        <td>Rp <?php echo number_format($total_jasa_esge);?></td>
                                                        <td>Rp <?php echo number_format($total_setoran_esge);?></td>
                                                    </tr>
                                                    <?php 
                                                    $total_masuk = 0;
                                                    $total_masuk_jasa = 0;
                                                    $total_masuk__total = 0;
                                                    foreach ($rekap_listrik as $key => $value) {
                                                        $total_masuk = $total_masuk + $value->balance;
                                                        $total_masuk_jasa = $total_masuk_jasa + ($value->income - $value->balance);
                                                        $total_masuk__total = $total_masuk__total + $value->income;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $value->name; ?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp <?php echo number_format($value->income - $value->balance);?></td>
                                                        <td>Rp <?php echo number_format($value->income);?></td>
                                                    </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>Titip Transfer</td>
                                                        <td>Rp <?php echo number_format($total_titip_transfer - $total_jasa_tt);?></td>
                                                        <td>Rp <?php echo number_format($total_jasa_tt);?></td>
                                                        <td>Rp <?php echo number_format($total_titip_transfer);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Penjualan</td>
                                                        <td>Rp <?php echo number_format($total_penjualan);?></td>
                                                        <td>Rp <?php echo number_format(0);?></td>
                                                        <td>Rp <?php echo number_format($total_penjualan);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>JET</td>
                                                        <td>Rp <?php echo number_format($total_jet_plus->total);?></td>
                                                        <td>Rp <?php echo number_format(0);?></td>
                                                        <td>Rp <?php echo number_format($total_jet_plus->total);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lain Lain</td>
                                                        <td>Rp <?php echo number_format($total_lain_plus->total);?></td>
                                                        <td>Rp <?php echo number_format(0);?></td>
                                                        <td>Rp <?php echo number_format($total_lain_plus->total);?></td>
                                                    </tr>
                                                </tbody>
                                                <?php 
                                                    $total_masuk = $total_masuk + $total_lain_plus->total + $total_jet_plus->total + $total_penjualan + ($total_titip_transfer - $total_jasa_tt) + ($total_setoran_esge - $total_jasa_esge);
                                                    $total_masuk_jasa = $total_masuk_jasa + $total_jasa_tt + $total_jasa_esge;
                                                    $total_masuk__total = $total_masuk__total + $total_lain_plus->total + $total_jet_plus->total + $total_penjualan + $total_titip_transfer + $total_setoran_esge;
                                               ?>
                                                <thead class="bg-secondary">
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>Rp <?php echo number_format($total_masuk);?></th>
                                                        <th>Rp <?php echo number_format($total_masuk_jasa);?></th>
                                                        <th>Rp <?php echo number_format($total_masuk__total);?></th>                                         
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            Rekap Uang Keluar
                                        </div>
                                        <div class="card-body">
                                            <table id="table_rekap_saldo" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Transaksi</th>
                                                        <th>Uang Keluar</th>                                     
                                                    </tr>
                                                </thead>
                                                <tbody>    
                                                    <tr>
                                                        <td>Tarik Tunai</td>
                                                        <td>Rp <?php echo number_format($total_tarik_tunai);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Setoran JET</td>
                                                        <td>Rp <?php echo number_format($total_jet_minus->total);?></td>
                                                    </tr>
                                                    <tr>
                                                        <?php 
                                                         $total_keluar = 0;
                                                            foreach ($total_lain_minus as $key => $value) {
                                                                $total_keluar = $total_keluar + $value->balance;
                                                        ?>
                                                        <td><?php echo $value->name; ?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <?php
                                                            }
                                                        ?>
                                                        
                                                    </tr>
                                                </tbody>
                                                <?php 
                                                    $total_keluar = $total_keluar + $total_jet_minus->total + $total_tarik_tunai;
                                                ?>
                                                <thead class="bg-secondary">
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>Rp <?php echo number_format($total_keluar);?></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card card-info">
                                        <div class="card-header">
                                            Rekap Uang
                                        </div>
                                        <div class="card-body">
                                            <table id="table_rekap_saldo" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Transaksi</th>
                                                        <th>Jumlah</th>                                     
                                                    </tr>
                                                </thead>
                                                <tbody>    
                                                    <tr>
                                                        <td>Uang Masuk</td>
                                                        <td>Rp <?php echo number_format($total_masuk);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jasa</td>
                                                        <td>Rp <?php echo number_format($total_masuk_jasa);?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Uang Keluar</td>
                                                        <td>Rp <?php echo number_format($total_keluar);?></td>
                                                    </tr>
                                                                                                    </tbody>
                                                <?php 
                                                    $total = $total_masuk + $total_masuk_jasa - $total_keluar;                                          ?>
                                                <thead class="bg-secondary">
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>Rp <?php echo number_format($total);?></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<script>
  function showModal(e){
		event.preventDefault();
		mdl = $('#myModal');
		mdl.modal('show');
		uri = $(e).attr('uri');
        dataid = $(e).attr('dataid');
        datatrx = $(e).attr('datatrx');
 		$.post(uri, {ajax: true, id: dataid, trx_id: datatrx}, function (data) {
			mdl.find("div[class=modal-content]").html(data);
            onPrepre();
		});
  }
  $( document ).ready(function() {
    setTimeout(function() {
        $('.alert').fadeOut(3000);
        }, 1000
    );
    $('#table_setoran_esge').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_setoran_esge);?></b><br><b>Total Jasa : Rp <?php echo number_format($total_jasa_esge);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_setoran_esge);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_titip_transfer').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_titip_transfer);?></b><br><b>Total Jasa : Rp <?php echo number_format($total_jasa_tt);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_titip_transfer);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_jet').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_jet);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_jet);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });
    
    $('#table_tektaya').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_tektaya);?></b><br><b>Total Jasa : Rp <?php echo number_format($jasa_tektaya);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_tektaya);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_lain').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_lain);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_lain);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_penjualan').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_penjualan);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_penjualan);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_tarik_tunai').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_tarik_tunai);?></b><br><b>Total Jasa : Rp <?php echo number_format($jasa_tarik);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_tarik_tunai);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });
    
    $( "#balance_date" ).change(function() {
        $('#filter').submit();
    })
  });

  function count_service(balance , income , target){
    var resBalance = balance.split(".");
    var new_balance = resBalance[0].split(',').join('')
    var resincome = income.split(".");
    var new_income = resincome[0].split(',').join('')
    service = new_income - new_balance;
    $('#'+target).val(service);
    $("#"+target).inputmask();
  }

  function count_total(price , count , target){
    var resPrice = price.split(".");
    var new_price = resPrice[0].split(',').join('')
    
    total = new_price * count;

    $('#'+target).val(total);
    $("#"+target).inputmask();
  }
</script>