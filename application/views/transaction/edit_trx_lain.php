<div class="modal-header bg-success">
    <h4 class="modal-title">Edit Transaction</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>transaction/save_trx_lain" method="POST" id="form_add">
<input type="hidden" name="action" value="edit">
<input type="hidden" name="account_id" value="<?php echo $trx->id; ?>">
<input type="hidden" name="trx_id" value="<?php echo $trx->trx_id; ?>">
<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label >Nama Transaksi</label>
                    <input type="text" value="<?php echo $trx->name; ?>" class="form-control" placeholder="Masukan Nama" name="name" required>
                </div>
                <div class="form-group">
                    <label >Jenis Transaksi</label>
                    <select class="form-control" name="trx_type" required>
                        <option value="">Pilih</option>
                        <option value="1" <?php if($trx->trx_type == 1){echo "selected";} ?>>Uang Masuk</option>
                        <?php if ($this->session->userdata("SesiRole") != "Customer Service"){ ?>
                        <option value="-1" <?php if($trx->trx_type == -1){echo "selected";} ?>>Uang Keluar</option>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                    <label >JUmlah</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" class="form-control" value="<?php echo $trx->balance; ?>"  id='balance' name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false" required>
                    </div>
                </div>
                <div class="form-group">
                    <label >Keterangan</label>
                    <input type="text" class="form-control" value="<?php echo $trx->note; ?>"  placeholder="Masukan Keterangan" name="note" required>
                </div>
            </div>
            <hr>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-success btn-sm" style="width: 100px;">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>
