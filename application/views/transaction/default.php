<style>
    .custom-info{
        white-space: nowrap;
        margin-top:30px ;
        display: none;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <h3 class="card-title"><b>Transaksi</b></h3>
                        </div>
                        <div class="col-md-12" style="padding-left: 20px;padding-right:20px;">
                            <?php if($this->session->flashdata('message') == "success"){ ?>
                            <div class="alert alert-success">
                                <strong>Success!</strong>
                            </div>
                            <?php }
                            if ($this->session->flashdata('message') == "failed"){
                            ?>
                            <div class="alert alert-danger">
                                <strong>Failed</strong>
                            </div>
                            <?php } ?>
                        </div>
                        <?php
                            $tab = $this->session->flashdata('tab');
                            $tab_esge = "";
                            $tab_titip = "";
                            $tab_tektaya = "";
                            $tab_sales = "";
                            $tab_tarik = "";
                            $tab_jet = "";
                            $tab_lain = "";

                            $tab_esge_list = "";
                            $tab_titip_list = "";
                            $tab_tektaya_list = "";
                            $tab_sales_list = "";
                            $tab_tarik_list = "";
                            $tab_jet_list = "";
                            $tab_lain_list = "";
                            switch ($tab) {
                                case 'esge':
                                    $tab_esge = "active";
                                    $tab_esge_list = "show active";
                                    break;
                                case 'titip':
                                    $tab_titip = "active";
                                    $tab_titip_list = "show active";
                                    break;
                                case 'tektaya':
                                    $tab_tektaya = "active";
                                    $tab_tektaya_list = "show active";
                                    break;
                                case 'sales':
                                    $tab_sales = "active";
                                    $tab_sales_list = "show active";
                                    break;
                                case 'tarik':
                                    $tab_tarik = "active";
                                    $tab_tarik_list = "show active";
                                    break;
                                case 'jet':
                                    $tab_jet = "active";
                                    $tab_jet_list = "show active";
                                    break;
                                case 'lain':
                                    $tab_lain = "active";
                                    $tab_lain_list = "show active";
                                    break;
                                default:
                                    $tab_esge = "active";
                                    $tab_esge_list = "show active";
                                    break;
                            }
                            
                        ?>
                        <div class="card-body">
                            <div class="card card-tabs card-info">
                                <div class="card-header p-0 pt-1">
                                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_esge; ?>" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Setoran RBR</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_titip; ?>" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Titip Transfer</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_tektaya; ?>" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Tektaya & Posfin</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_sales; ?>" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Penjualan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_tarik; ?>" id="tabs_tarik_tunai-tab" data-toggle="pill" href="#tabs_tarik_tunai" role="tab" aria-controls="tabs_tarik_tunai" aria-selected="false">Tarik Tunai</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_jet; ?>" id="tabs_jet-tab" data-toggle="pill" href="#tabs_jet" role="tab" aria-controls="tabs_jet" aria-selected="false">JET Express</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo $tab_lain; ?>" id="tabs_lain-tab" data-toggle="pill" href="#tabs_lain" role="tab" aria-controls="tabs_lain" aria-selected="false">Lain lain</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-one">
                                        <div class="tab-pane fade <?php echo $tab_esge_list; ?>" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                            <table id="table_setoran_esge" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Saldo Terpotong</th>
                                                        <th>Jasa</th>
                                                        <th>Uang Masuk</th> 
                                                        <th>Jam Trx</th> 
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                                
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_setoran_esge = 0;
                                                        foreach ($setoran_esge as $key => $value) {
                                                            $total_setoran_esge = $total_setoran_esge + $value->total;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp. <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                        <td><button uri = "<?php echo BASE_URL; ?>transaction/edit_esge" dataid="<?php echo $value->trx_account_id; ?>" datatrx="<?php echo $value->trx_id; ?>" class="btn btn-success btn-sm" onclick="showModal(this);"><span class="fa fas fa-edit"></button> <button class="btn btn-danger btn-sm" onclick="showModal(this);" uri = "<?php echo BASE_URL; ?>transaction/delete_esge" dataid="<?php echo $value->trx_account_id; ?>" datatrx="<?php echo $value->trx_id; ?>"><span class="fa fas fa-trash"></span></button></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" onclick="showModal(this);" bg="bg-info"  uri = "<?php echo BASE_URL; ?>transaction/add_esge"> <i class="nav-icon fa fa-cart-plus"></i> Add Transaksi</button>
                                        </div>
                                        <div class="tab-pane fade <?php echo $tab_titip_list; ?>" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group input-group-sm" style="width:200px;">
                                                        <select class="form-control" Id = "BankTTFilter">
                                                            <option value="All Status">All Bank</option>
                                                            <?php 
                                                                foreach ($banks as $key => $value) {
                                                                    echo '<option id="'.$value->id.'" value="'.$value->name.'">'.$value->name.'</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="pull-right" style="float: right;">
                                                        <div class="input-group input-group-sm"  style="width:200px;">
                                                            <input type="text" class="form-control" placeholder="Search" id="BankTTSearch" >
                                                                <span class="input-group-btn">
                                                                <button type="button" class="btn btn-sm btn-default" style="margin-left: -3px;"><i class="fa fa-search"></i></button>
                                                                </span>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                            <table id="table_titip_transfer" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Bank</th>
                                                        <th>Jenis Transaksi</th>
                                                        <th>Saldo Terpotong</th>
                                                        <th>Jasa</th>
                                                        <th>Uang Masuk</th> 
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                           
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_titip_transfer = 0;
                                                        foreach ($titip_transfer as $key => $value) {
                                                            $total_titip_transfer = $total_titip_transfer + $value->total;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td><?php echo $value->bank;?></td>
                                                        <td><?php echo $value->transaction_type;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp. <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                        <td><button uri = "<?php echo BASE_URL; ?>transaction/edit_titip_transfer" dataid="<?php echo $value->trx_bank_id; ?>" datatrx="<?php echo $value->trx_id; ?>" class="btn btn-success btn-sm" onclick="showModal(this);"><span class="fa fas fa-edit"></span></button> <button  onclick="showModal(this);" uri = "<?php echo BASE_URL; ?>transaction/delete_titip_transfer" dataid="<?php echo $value->trx_bank_id; ?>" datatrx="<?php echo $value->trx_id; ?>"  class="btn btn-danger btn-sm" ><span class="fa fas fa-trash"></span></button></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" onclick="showModal(this);" bg="bg-info"  uri = "<?php echo BASE_URL; ?>transaction/add_titip_transfer"> <i class="nav-icon fa fa-cart-plus"></i> Add Transaksi</button>
                                        </div>
                                        <div class="tab-pane fade <?php echo $tab_tektaya_list; ?>" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group input-group-sm" style="width:200px;">
                                                        <select class="form-control" Id = "TektayaFilter">
                                                            <option value="All Status">All Account</option>
                                                            <?php 
                                                                foreach ($accounts as $key => $value) {
                                                                    echo '<option total="'.number_format($value->total).'" value="'.$value->name.'">'.$value->name.'</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="pull-right" style="float: right;">
                                                        <div class="input-group input-group-sm"  style="width:200px;">
                                                            <input type="text" class="form-control" placeholder="Search" id="TektayaSearch" >
                                                                <span class="input-group-btn">
                                                                <button type="button" class="btn btn-sm btn-default" style="margin-left: -3px;"><i class="fa fa-search"></i></button>
                                                                </span>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <table id="table_tektaya" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Account</th>
                                                        <th>Saldo Terpotong</th>
                                                        <th>Jasa</th>
                                                        <th>Uang Masuk</th> 
                                                        <th>Jam Trx</th>     
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                            
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_tektaya = 0;
                                                        foreach ($tektaya as $key => $value) {
                                                            $total_tektaya = $total_tektaya + $value->total;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td><?php echo $value->account_name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp. <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>                                                       
                                                        <td><button class="btn btn-success btn-sm" uri = "<?php echo BASE_URL; ?>transaction/edit_tektaya" dataid="<?php echo $value->trx_account_id; ?>" datatrx="<?php echo $value->trx_id; ?>" onclick="showModal(this);" ><span class="fa fas fa-edit"></button> <button class="btn btn-danger btn-sm" uri = "<?php echo BASE_URL; ?>transaction/delete_tektaya" dataid="<?php echo $value->trx_account_id; ?>" datatrx="<?php echo $value->trx_id; ?>" onclick="showModal(this);"><span class="fa fas fa-trash"></span></button></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" onclick="showModal(this);" bg="bg-info"  uri = "<?php echo BASE_URL; ?>transaction/add_tektaya"> <i class="nav-icon fa fa-cart-plus"></i> Add Transaksi</button>    
                                        </div> 
                                        <div class="tab-pane fade <?php echo $tab_sales_list; ?>" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                                        <table id="table_penjualan" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga Satuan</th>
                                                        <th>Banyaknya</th>
                                                        <th>Total</th> 
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                              
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_penjualan = 0;
                                                        foreach ($penjualan as $key => $value) {
                                                            $total_penjualan = $total_penjualan + $value->total;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->price);?></td>
                                                        <td><?php echo $value->count;?></td>
                                                        <td>Rp <?php echo number_format($value->total);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                        <td><button class="btn btn-success btn-sm" uri = "<?php echo BASE_URL; ?>transaction/edit_penjualan" dataid="<?php echo $value->trx_sales_id; ?>" datatrx="<?php echo $value->trx_id; ?>" onclick="showModal(this);"><span class="fa fas fa-edit"></button> <button class="btn btn-danger btn-sm" uri = "<?php echo BASE_URL; ?>transaction/delete_penjualan" dataid="<?php echo $value->trx_sales_id; ?>" datatrx="<?php echo $value->trx_id; ?>" onclick="showModal(this);"><span class="fa fas fa-trash"></span></button></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" onclick="showModal(this);" bg="bg-info"  uri = "<?php echo BASE_URL; ?>transaction/add_penjualan"> <i class="nav-icon fa fa-cart-plus"></i> Add Transaksi</button>
                                        </div>
                                        <div class="tab-pane fade <?php echo $tab_tarik_list; ?>" id="tabs_tarik_tunai" role="tabpanel" aria-labelledby="tabs_tarik_tunai">
                                            <table id="table_tarik_tunai" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Bank</th>
                                                        <th>Tarik Tunai</th>
                                                        <th>Jasa</th>
                                                        <th>Total Tarik Tunai</th>
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                           
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_tarik_tunai = 0;
                                                        foreach ($tarik_tunai as $key => $value) {
                                                            $total_tarik_tunai = $total_tarik_tunai + $value->income;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td><?php echo $value->bank;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp <?php echo number_format($value->service_fee);?></td>
                                                        <td>Rp <?php echo number_format($value->income);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                        <td><button uri = "<?php echo BASE_URL; ?>transaction/edit_tarik_tunai" dataid="<?php echo $value->trx_bank_id; ?>" datatrx="<?php echo $value->trx_id; ?>" class="btn btn-success btn-sm" onclick="showModal(this);"><span class="fa fas fa-edit"></span></button> <button  onclick="showModal(this);" uri = "<?php echo BASE_URL; ?>transaction/delete_tarik_tunai" dataid="<?php echo $value->trx_bank_id; ?>" datatrx="<?php echo $value->trx_id; ?>"  class="btn btn-danger btn-sm" ><span class="fa fas fa-trash"></span></button></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" uri="<?php echo BASE_URL; ?>transaction/add_tarik_tunai" onclick="showModal(this)"> <i class="nav-icon fa fa-cart-plus"></i> Add Tarik Tunai</button>
                                        </div>
                                        <div class="tab-pane fade <?php echo $tab_jet_list; ?>" id="tabs_jet" role="tabpanel" aria-labelledby="tabs_jet">
                                            <table id="table_jet" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama / No Resi</th>
                                                        <th>Jasa</th>
                                                        <th>Total</th>
                                                        <th>Jam Trx</th>   
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                            
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_jet = 0;
                                                        foreach ($jet as $key => $value) {
                                                            $total_jet = $total_jet + $value->balance;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                        <td><button uri = "<?php echo BASE_URL; ?>transaction/edit_jet" dataid="<?php echo $value->trx_courier_id; ?>" datatrx="<?php echo $value->trx_id; ?>" class="btn btn-success btn-sm" onclick="showModal(this);"><span class="fa fas fa-edit"></span></button> <button  onclick="showModal(this);" uri = "<?php echo BASE_URL; ?>transaction/delete_jet" dataid="<?php echo $value->trx_courier_id; ?>" datatrx="<?php echo $value->trx_id; ?>"  class="btn btn-danger btn-sm" ><span class="fa fas fa-trash"></span></button></td>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" uri="<?php echo BASE_URL; ?>transaction/add_jet" onclick="showModal(this)"> <i class="nav-icon fa fa-cart-plus"></i> Add Transaksi</button> <button  class="btn btn-warning btn-sm" uri="<?php echo BASE_URL; ?>transaction/add_setoran_jet" onclick="showModal(this)"> <i class="nav-icon fa fa-cart-plus"></i> Add Setoran Jet</button>
                                        </div>
                                        <div class="tab-pane fade <?php echo $tab_lain_list; ?>" id="tabs_lain" role="tabpanel" aria-labelledby="tabs_lain">
                                            <table id="table_lain" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Total</th>
                                                        <th>Jam Trx</th>  
                                                        <th>Keterangan</th> 
                                                        <th>Created</th>
                                                        <th>Last Update</th>                                           
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>           
                                                    <?php
                                                        $i = 1;
                                                        $total_lain = 0;
                                                        foreach ($trx_lain as $key => $value) {
                                                            $total_lain = $total_lain + $value->balance;
                                                    ?> 
                                                    <tr>
                                                        <td><?php echo $i;?></td>
                                                        <td><?php echo $value->name;?></td>
                                                        <td>Rp <?php echo number_format($value->balance);?></td>
                                                        <td><?php echo $value->time;?></td>
                                                        <td><?php echo $value->note;?></td>
                                                        <td><?php echo $value->created;?></td>
                                                        <td><?php echo $value->updated;?></td>
                                                        <?php
                                                            if($value->name == "#Transfer Uang"){
                                                                echo "<td>";
                                                            }else{
                                                        ?>
                                                        
                                                        <td><button  class="btn btn-success btn-sm" uri = "<?php echo BASE_URL; ?>transaction/edit_trx_lain" dataid="<?php echo $value->id; ?>" datatrx="<?php echo $value->trx_id; ?>" onclick="showModal(this)"><span class="fa fas fa-edit"></button> <button class="btn btn-danger btn-sm" uri = "<?php echo BASE_URL; ?>transaction/delete_trx_lain" dataid="<?php echo $value->id; ?>" datatrx="<?php echo $value->trx_id; ?>" onclick="showModal(this)"><span class="fa fas fa-trash"></span></button></td>
                                                            <?php } ?>
                                                    </tr>
                                                    <?php
                                                        $i ++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table><br>
                                            <button class="btn btn-info btn-sm" uri="<?php echo BASE_URL; ?>transaction/add_trx_lain" onclick="showModal(this)" > <i class="nav-icon fa fa-cart-plus"></i> Add Transaksi Lain Lain</button>
                                            <button class="btn btn-warning btn-sm" style="color:#fff" uri="<?php echo BASE_URL; ?>transaction/add_trx_transfer_uang" onclick="showModal(this)" > <i class="nav-icon fa fa-cart-plus"></i> Transfer Uang ke User Lain</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            <!-- /.card -->
                            </div>
			    <?php if ($this->session->userdata("SesiRole") != "Owner"){ ?>
                            <button <?php if ($total_uang-$total_uang_setor == 0){echo "Disabled";} ?> class="btn btn-info btn-sm float-right" uri="<?php echo BASE_URL; ?>transaction/add_setoran" onclick="showModal(this)" >Setorkan</button>
                            <?php } ?>
			    <p><b>Total Uang Masuk : Rp  <?php echo number_format($total_uang); ?></b></p>
                            <?php if ($total_uang_setor != 0){ ?>
                            <p><b>Sudah di setor : Rp  <?php echo number_format($total_uang_setor); ?> Status <span class="badge badge-warning">New Setoran</span></b></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<script>
  function showModal(e){
		event.preventDefault();
		mdl = $('#myModal');
		mdl.modal('show');
		uri = $(e).attr('uri');
        dataid = $(e).attr('dataid');
        datatrx = $(e).attr('datatrx');
 		$.post(uri, {ajax: true, id: dataid, trx_id: datatrx}, function (data) {
			mdl.find("div[class=modal-content]").html(data);
            onPrepre();
		});
  }
  $( document ).ready(function() {
    setTimeout(function() {
        $('.alert').fadeOut(3000);
        }, 1000
    );
    $('#table_setoran_esge').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_setoran_esge);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_setoran_esge);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_titip_transfer').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b id='total_titip_transfer_filter'></b> <b>Total : Rp <?php echo number_format($total_titip_transfer);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_titip_transfer);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

        $('#table_titip_transfer_filter').removeClass('dataTables_filter');
        $('#table_titip_transfer_filter').addClass('custom-info');

        $('[data-toggle="tooltip"]').tooltip();
        
        $('#BankTTFilter').on('change', function() {
            var status = $('#BankTTFilter').val();
            var element = $(this).find('option:selected'); 
            var bank = element.attr("id"); 
            var MitraTable = $('#table_titip_transfer').DataTable(); 
            if(status == "All Status"){
                MitraTable.search('').draw(); 
            }else{
                MitraTable.search(status).draw(); 

                var postForm = { 
                    'bank_id'     : bank
                };
                
                $.ajax({ 
                    type      : 'POST', 
                    url       : '<?php echo BASE_URL; ?>transaction/getTotalTrxByBank', 
                    data      : postForm, 
                    dataType  : 'json',
                    success   : function(data) {
                        $('#total_titip_transfer_filter').html('Total '+status+' : Rp '+data.total+' <br>');  
                    }
                });
            }
        });
        $("#BankTTSearch").keyup(function(){
            var search = $('#BankTTSearch').val();
            var MitraTable = $('#table_titip_transfer').DataTable(); 
            MitraTable.search($('#BankTTSearch').val()).draw(); 
        });

    $('#table_jet').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_jet);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_jet);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });
    
    $('#table_tektaya').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b id='total_tektaya_filter'></b> <b>Total : Rp <?php echo number_format($total_tektaya);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_tektaya);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    
    $('#table_tektaya_filter').removeClass('dataTables_filter');
    $('#table_tektaya_filter').addClass('custom-info');
    $('#TektayaFilter').on('change', function() {
        var status = $('#TektayaFilter').val();
        var element = $(this).find('option:selected'); 
        var total = element.attr("total"); 
        var TektayaTable = $('#table_tektaya').DataTable(); 
        if(status == "All Status"){
            TektayaTable.search('').draw(); 
        }else{
            TektayaTable.search(status).draw(); 

           $('#total_tektaya_filter').html('Total '+status+' : Rp '+total+' <br>');
        }
    });
    $("#TektayaSearch").keyup(function(){
        var search = $('#TektayaSearch').val();
        var TektayaTable = $('#table_tektaya').DataTable(); 
        TektayaTable.search($('#TektayaSearch').val()).draw(); 
    });


    $('#table_lain').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_lain);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_lain);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_penjualan').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_penjualan);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_penjualan);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });

    $('#table_tarik_tunai').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength":5,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "oLanguage": {
                    "sInfo": "<b>Total : Rp <?php echo number_format($total_tarik_tunai);?></b>",
                    "sInfoFiltered": "",
                    "sInfoEmpty":"<b>Total : Rp <?php echo number_format($total_tarik_tunai);?></b>",
                    "oPaginate": {
                        "sPrevious": "<i class='fa fa-angle-left'></i>",
                        "sNext":"<i class='fa fa-angle-right'></i>"
                    }
            }
    });
    

  });

  function count_service(balance , income , target){
    var resBalance = balance.split(".");
    var new_balance = resBalance[0].split(',').join('')
    var resincome = income.split(".");
    var new_income = resincome[0].split(',').join('')
    service = new_income - new_balance;
    $('#'+target).val(service);
    $("#"+target).inputmask();
  }

  function count_total(price , count , target){
    var resPrice = price.split(".");
    var new_price = resPrice[0].split(',').join('')
    
    total = new_price * count;

    $('#'+target).val(total);
    $("#"+target).inputmask();
  }
</script>