<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TransactionModel extends CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
    }

    function getBank(){
        $query = 'select id, name from mtr_bank_accounts';
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTotalTrxBankBackup($id){
        $query ="select ac.id, ac.name ,  ifnull((select sum(c.amount)
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = curdate() and b.trx_type = -1 and c.user_id = ".$id." and d.id = ac.id ),0) as total from mtr_bank_accounts ac
        ";
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTotalTrxBank($id,$bank){
        $query ="select ifnull(sum(c.amount),0) as total from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        where date(c.create_at) = curdate() and b.trx_type = -1 and c.user_id = ".$id." AND b.bank_id = ".$bank;
        $result = $this->db->query($query)->row();
        return $result;
    }

    

    function getTotalTrxAccount($id, $trx_type){
        $query ="select ma.id , ma.name ,
        ifnull((select sum(c.amount )as total
                from trx_transaction_account a
                left join trx_account b on a.trx_account_id = b.id 
                left join trx_transaction c on a.trx_id = c.id 
                where date(c.create_at) = curdate() and b.account_id = ma.id and c.user_id = ".$id."),0) as total
        from mtr_accounts ma where ma.account_type = ".$trx_type;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getSetoranEsge($user_id){
        $query = "select a.trx_id , a.trx_account_id , b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        where date(c.create_at) = curdate() and b.account_id = 1 and c.user_id = ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getSetoranEsgeDetail($trx_id , $trx_account_id){
        $query = "select a.trx_id , a.trx_account_id , b.account_id, b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        where a.trx_account_id = ".$trx_account_id." and a.trx_id = ".$trx_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTektaya($user_id){
        $query = "select d.name as account_name, a.trx_id , a.trx_account_id , b.name, b.amount as balance , (c.amount - b.amount ) as service_fee ,
        c.amount as income, c.amount as total,  b.trx_time as time , b.created_by as created , b.updated_by as updated
        from trx_transaction_account a
        left join trx_account b on a.trx_account_id = b.id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_accounts d on b.account_id = d.id
        where date(c.create_at) = curdate() and d.account_type = 2 and c.user_id = ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTitipTransfer($user_id){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (c.amount - b.amount )as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = curdate() and b.trx_type = -1 and c.user_id = ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTitipTransferDetail($trx_id , $trx_account_id){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.id as bank_id ,d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (c.amount - b.amount )as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where a.trx_bank_id = ".$trx_account_id." and a.trx_id = ".$trx_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTotalUang($user_id){
        $query = "select sum(trx_type * amount) as total from trx_transaction tt where user_id = ".$user_id." and date(create_at) = curdate() ";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTotalUangBelumSetor($user_id){
        $query = "select sum(trx_type * amount) as total from 
        trx_transaction tt where user_id = ".$user_id." and date(create_at) = curdate() and id not in  
        (select a.trx_id from trx_setoran_details a left join trx_setoran b on a.setoran_id = b.id where b.from = ".$user_id.")";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTotalUangSetor($user_id){
        $query = "select ifnull(sum(c.amount * c.trx_type ),0) as total from trx_setoran_details a
        left join trx_setoran b on b.id = a.setoran_id 
        left join trx_transaction c on a.trx_id = c.id 
        where b.status = 10 and date(c.create_at) = curdate() and b.from = ".$user_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTarikTunai($user_id){
        $query = "select a.trx_id , a.trx_bank_id, b.name , d.name as bank , b.`transaction` as transaction_type,
        b.amount as balance ,  c.amount as income , b.trx_time as time ,
        (b.amount - c.amount)as service_fee , c.amount as total, b.created_by as created , b.updated_by as updated
        from trx_transaction_bank a 
        left join trx_bank b on b.id = a.trx_bank_id 
        left join trx_transaction c on a.trx_id = c.id 
        left join mtr_bank_accounts d on b.bank_id = d.id 
        where date(c.create_at) = curdate() and b.trx_type = 1 and c.user_id = ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getPenjualan($user_id){
        $query = "select a.trx_id , a.trx_sales_id , b.name , b.price , b.qty as count , b.total , time(b.create_at) as `time` , 
        b.created_by as created , b.updated_by as updated 
        from  trx_transaction_sales a 
        left join trx_sales b on b.id = a.trx_sales_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = curdate() and c.user_id = ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getPenjualanDetail($trx_sales_id,$trx_id){
        $query = "select a.trx_id , a.trx_sales_id , b.name , b.price , b.qty as count , b.total , time(b.create_at) as `time` , 
        b.created_by as created , b.updated_by as updated 
        from  trx_transaction_sales a 
        left join trx_sales b on b.id = a.trx_sales_id 
        left join trx_transaction c on c.id = a.trx_id
        where a.trx_sales_id = ".$trx_sales_id." and a.trx_id = ".$trx_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getJet($user_id){
        $query = "select a.trx_id , a.trx_courier_id , b.name , (b.trx_type * b.amount) as balance ,
        time(b.created_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_courier a 
        left join trx_courier b on b.id = a.trx_courier_id 
        left join trx_transaction c on c.id = a.trx_id
        where date(c.create_at) = curdate() and c.user_id =  ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getJetDetail($trx_courier_id,$trx_id){
        $query = "select a.trx_id , a.trx_courier_id , b.name , b.trx_type, b.amount as balance ,
        time(b.created_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_courier a 
        left join trx_courier b on b.id = a.trx_courier_id 
        left join trx_transaction c on c.id = a.trx_id
        where a.trx_courier_id = ".$trx_courier_id." and a.trx_id = ".$trx_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTrxLain($user_id){
        $query = "select a.id, a.trx_id , a.note , b.name , (b.trx_type * b.amount) as balance ,
        time(b.create_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_lain a 
        left join trx_transaction b on b.id = a.trx_id
        where date(b.create_at) = curdate() and b.user_id =  ".$user_id;
        $result = $this->db->query($query)->result();
        return $result;
    }

    function getTrxLainDetail($trx_id, $trx_account_id){
        $query = "select a.id, a.trx_id , a.note , b.name , b.trx_type,  b.amount as balance ,
        time(b.create_at) as `time` , b.created_by as created , b.updated_by as updated 
        from  trx_transaction_lain a 
        left join trx_transaction b on b.id = a.trx_id
        where a.trx_id = ".$trx_id." and a.id = ".$trx_account_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function getTotalTransaction($user_id){
        $query ="select ifnull(sum(amount * trx_type),0) as total from trx_transaction tt where date(create_at )=curdate() and user_id = ".$user_id;
        $result = $this->db->query($query)->row();
        return $result;
    }

    function add_transaction_account($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income ){
        $query = "CALL add_transaction_account(".$user_id.",".$account_id.",".$balance.",-1,
        '".$user_name."','".$transaction_name."','".$trx_time."',true,1,".$income.")";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function edit_transaction_account($trx_account_id,$trx_id,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income ){
        $query = "CALL edit_transaction_account(".$trx_account_id.", ".$trx_id.", ".$user_id.",".$account_id.",".$balance.",-1,
        '".$user_name."','".$transaction_name."','".$trx_time."',true,1,".$income.")";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function delete_transaction_account($trx_account_id,$trx_id){
        $query = "CALL delete_transaction_account(".$trx_account_id.",".$trx_id.",-1,true)";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function add_transaction_bank($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income ,$trx_tipe){
        $query = "CALL add_transaction_bank(".$user_id.",".$account_id.",".$balance.",-1,
        '".$user_name."','".$transaction_name."','".$trx_time."',true,1,".$income.", '".$trx_tipe."')";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function edit_transaction_bank($trx_account_id,$trx_id,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income,$trx_tipe ){
        $query = "CALL edit_transaction_bank(".$trx_account_id.", ".$trx_id.", ".$user_id.",".$account_id.",".$balance.",-1,
        '".$user_name."','".$transaction_name."','".$trx_time."',true,1,".$income.", '".$trx_tipe."')";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function delete_transaction_bank($trx_account_id,$trx_id){
        $query = "CALL delete_transaction_bank(".$trx_account_id.",".$trx_id.",-1,true)";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function add_tarik_tunai($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income ,$trx_tipe){
        $query = "CALL add_transaction_bank(".$user_id.",".$account_id.",".$balance.",1,
        '".$user_name."','".$transaction_name."','".$trx_time."',true,-1,".$income.", '".$trx_tipe."')";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function edit_tarik_tunai($trx_account_id,$trx_id,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income,$trx_tipe ){
        $query = "CALL edit_transaction_bank(".$trx_account_id.", ".$trx_id.", ".$user_id.",".$account_id.",".$balance.",1,
        '".$user_name."','".$transaction_name."','".$trx_time."',true,-1,".$income.", '".$trx_tipe."')";
        $result = $this->db->query($query)->row();
        return $result;
    }

    function delete_tarik_tunai($trx_account_id,$trx_id){
        $query = "CALL delete_transaction_bank(".$trx_account_id.",".$trx_id.",1,true)";
        $result = $this->db->query($query)->row();
        return $result;
    }

    

    function add_transaction_sales($user_id,$total,$user_name,$transaction_name, $qty, $price){
       $query = "CALL add_transaction_sales(".$user_id.",".$total.",-1,'".$user_name."','".$transaction_name."',1,".$total.",".$qty.",".$price.")";
       $result = $this->db->query($query)->row();
       return $result;
    }

    function edit_transaction_sales($trx_account_id,$trx_id,$user_id,$total,$user_name,$transaction_name, $qty, $price){
        $query = "CALL edit_transaction_sales(".$trx_account_id.", ".$trx_id.", ".$user_id.",".$total.",-1,'".$user_name."','".$transaction_name."',1,".$total.",".$qty.",".$price.")";
        $result = $this->db->query($query)->row();
        return $result;
     }

     function delete_transaction_sales($trx_account_id,$trx_id){
        $query = "CALL delete_transaction_sales(".$trx_account_id.", ".$trx_id.")";
        $result = $this->db->query($query)->row();
        return $result;
     }

     function add_transaction_courier($user_id,$total,$type_trx,$user_name,$transaction_name){
        $query = "CALL add_transaction_courier(".$user_id.",".$total.",".$type_trx.",'".$user_name."','".$transaction_name."',".$type_trx.",".$total.")";
        $result = $this->db->query($query)->row();
        return $result;
     }

     function edit_transaction_courier($trx_account_id,$trx_id,$user_id,$total,$type_trx,$user_name,$transaction_name){
        $query = "CALL edit_transaction_courier(".$trx_account_id.",".$trx_id.", ".$user_id.",".$total.",".$type_trx.",'".$user_name."','".$transaction_name."',".$type_trx.",".$total.")";
        $result = $this->db->query($query)->row();
        return $result;
     }
 
    function delete_transaction_courier($trx_account_id,$trx_id){
         $query = "CALL delete_transaction_courier(".$trx_account_id.", ".$trx_id.")";
         $result = $this->db->query($query)->row();
         return $result;
    }
    function add_transaction_lain($user_id,$total,$type_trx,$user_name,$transaction_name,$note){
        $query = "CALL add_transaction_lain(".$user_id.", '".$user_name."','".$transaction_name."',".$type_trx.",".$total.", '".$note."')";
        $result = $this->db->query($query)->row();
        return $result;
     }

     function edit_transaction_lain($trx_account_id,$trx_id,$user_id,$total,$type_trx,$user_name,$transaction_name,$note){
        $query = "CALL edit_transaction_lain(".$trx_account_id.", ".$trx_id.",".$user_id.", '".$user_name."','".$transaction_name."',".$type_trx.",".$total.", '".$note."')";
        $result = $this->db->query($query)->row();
        return $result;
     }
 
    function delete_transaction_lain($trx_account_id,$trx_id){
         $query = "CALL delete_transaction_lain(".$trx_account_id.", ".$trx_id.")";
         $result = $this->db->query($query)->row();
         return $result;
    }

    function add_setoran($user_id,$to_id){
        $query = "CALL add_setoran(".$user_id.", ".$to_id.")";
        $result = $this->db->query($query)->row();
        return $result;
     }




}