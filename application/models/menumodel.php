<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MenuModel extends CI_Model {

    public function __construct()
	{
		parent::__construct();
		
    }

    public function isLogin(){
        $controller = $this->uri->segment(1);
        $isLogin    = $this->session->userdata('LoggedIn');
        if(!$isLogin){
			$this->session->sess_destroy();
			if($controller!='login'){
				redirect('/login', 'refresh');
			}
		}
    }
}