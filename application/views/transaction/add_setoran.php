<div class="modal-header bg-info">
    <h4 class="modal-title">Setoran</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>transaction/save_setoran" method="POST" id="form_add">
<input type="hidden" name="action" value="add">
<div class="modal-body">
    <div class="col-md-12">
        <div class="form-group">
            <?php  if ($role == "Customer Service"){?>
            <label>Kasir</label>
            <?php }?>
            <?php  if ($role == "Kasir"){?>
            <label>Owner</label>
            <?php }?>
            <select class="form-control" name="setor_id" required>
                <option value="">Pilih</option>
                <?php foreach ($users as $key => $value) {
                    echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label >Total Uang</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" readonly value="<?php echo $total; ?>" class="form-control" id='total_income' name="total_income" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false">
            </div>
        </div>
        
    </div>
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-info btn-sm" style="width: 100px;">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#total_income").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>
