
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12" >
                    <div class="card"  >
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Add User</h3>
                            </div>
                        </div>
                        <div class="card-body">
                        <form method="post" action="<?php echo BASE_URL;?>user/save">
                            <div class="form-group">
                                <label for="Username">Username</label>
                                <input type="text" class="form-control" name="Username" id="Username" placeholder="Enter Username" required>
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" class="form-control" name="Password" id="Password" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" name="Name" id="Name" placeholder="Enter Name" required>
                            </div>
                            <div class="form-group">
                                <label for="Role">Role</label>
                                <select class="form-control" name="Role" id="Role" required>
                                    <option value="Owner">Owner</option>
                                    <option value="Kasir">Kasir</option>
                                    <option value="Customer Service">Customer Service</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-info">Submit</button> 
                        </form>
                        </div>
                    </div>
                </div>

            </div>
        <!-- /.row -->
        </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->