<?php
class dashboard extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        isLogin();
        $this->load->model("DashboardModel");
    }

    public function index(){
        
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "dashboard";
        $data['user_balance'] = $this->DashboardModel->getBalanceUser();
        $data['account_balance'] = $this->DashboardModel->getAccountBalance();
		$data['content'] = 'dashboard/default';
		$this->load->view('template',$data);
    }
}