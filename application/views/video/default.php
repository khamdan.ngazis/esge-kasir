
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12" >
                    <div class="card"  >
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Manage Video</h3>
                            </div>
                        </div>
                        
                        <div class="card-body">
                        <?php 
                                    if ($this->session->flashdata("message") != ""){
                                        echo $this->session->flashdata("message");
                                    } 
                                ?>
                        <form method="post" action="<?php echo BASE_URL;?>video/save">
                            <div class="form-group">
                                <label for="Name">Url Youtube Vudeo</label>
                            </div>
                            <div class="input-group mb-3" style="margin-top:-10px;">
                                <input type="text" class="form-control" name="value" id="video" value = "<?php echo $config->config_value; ?>" disabled>
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="button" Id="edit">Edit</button>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info" disabled id="save">Save</button> 
                        </form>
                        </div>
                    </div>
                </div>

            </div>
        <!-- /.row -->
        </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function () {
        window.setTimeout(function() {
            $(".alert").fadeTo(8000, 0).slideUp(1000, function(){
                $(this).remove();
            });
        }, 0);
        $('#edit').click(function(){
            $('#save').removeAttr('disabled');
            $('#video').removeAttr('disabled');
            $('#edit').attr('disabled' , 'disabled');
        });   
    });
</script>