<div class="modal-header bg-warning">
    <h4 class="modal-title">Add Transaction</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>transaction/save_jet" method="POST" id="form_add">
<input type="hidden" name="action" value="add">
<input type="hidden" name="trx_type" value="-1">
<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label >Setoran JET</label>
                    <input type="text" value="Setoran JET" readonly class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label >Jumlah Setoran</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" class="form-control" id='balance' name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false" required>
                    </div>
                </div>
            
            </div>
            <hr>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" id="submit" class="btn btn-warning btn-sm" style="width: 100px;">Save</button>
</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>
