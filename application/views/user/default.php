
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">List User</h3>
                                <a href="<?php echo BASE_URL;?>user/add"><button class="btn btn-sm btn-info"><span class="fas fa-plus"></span> Add User</button></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12" style="padding:0">
                                <?php 
                                    if ($this->session->flashdata("message") != ""){
                                        echo $this->session->flashdata("message");
                                    } 
                                ?>
                            </div> 
                            <table id="TblListUser" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Username</th>
                                        <th>Name</th>
                                        <th>role</th>
                                        <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($users as $key => $user) {
                                    ?>
                                    <tr>
                                        <td><?php echo $user->id; ?>
                                        <td><?php echo $user->username;?>
                                        <td><?php echo $user->name;?>
                                        <td><?php echo $user->role;?>
                                        <td><a href="<?php echo BASE_URL.'user/edit/'.$user->id; ?>"><button class="btn btn-sm btn-success" ><span class="fa fas fa-edit"></span></button></a> <button class="btn btn-sm btn-danger" onclick="deleteUser(<?php echo $user->id; ?>)"><span class="fa fas fa-trash"></span></button>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>     
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.row -->
        </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function() {

        window.setTimeout(function() {
            $(".alert").fadeTo(8000, 0).slideUp(1000, function(){
                $(this).remove();
            });
        }, 0);

        $('#TblListUser').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
        });

    });

    function deleteUser(id){
        var r = confirm("Are you sure ?");
        if (r == true) {
            location.replace("<?php echo BASE_URL;?>user/delete/"+id);
        } 
    }
</script>