<?php
class setoran extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("SetoranModel");
        $this->load->model("UserModel");
        $this->load->model("TransactionModel");
        
        isLogin();
    }

    public function index(){
        $user_id = $this->session->userdata("SesiUserId");
        $data['setoran'] = $this->SetoranModel->getSetoran($user_id);
        $data['terima'] = $this->SetoranModel->getTerima($user_id);
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "setoran";
		$data['content'] = 'setoran/default';
		$this->load->view('template',$data);
    }

    public function edit_setoran(){
        $id = $this->input->post('id');
        $data['trx'] = $this->SetoranModel->getSetoranDetail($id);
        $role = $this->session->userdata("SesiRole");
        if ($role == "Customer Service"){
            $role_setor = "Kasir";
        }
        if($role == "Kasir"){
            $role_setor = "Owner";
        }
        $data['link'] = "transaksi";
        $data['role'] = $role;
        $data['users'] = $this->UserModel->getUserbyRole($role_setor);
        $this->load->view('setoran/edit_setoran',$data);
    }

    public function terima_setoran(){
        $id = $this->input->post('id');
        $data['trx'] = $this->SetoranModel->getSetoranDetail($id);
        $role = $this->session->userdata("SesiRole");
        if ($role == "Owner"){
            $role_setor = "Kasir";
        }
        if($role == "Kasir"){
            $role_setor = "Customer Service";
        }
        $data['link'] = "transaksi";
        $data['role'] = $role;
        $data['users'] = $this->UserModel->getUserbyRole($role_setor);
        $this->load->view('setoran/terima_setoran',$data);
    }

    public function save_setoran(){
        $user_id = $this->session->userdata("SesiUserId");
        $id = $this->input->post('id');
        $to_id = $this->input->post('setor_id');
        $action = $this->input->post('action');
        if($action == "terima"){
            $save = $this->SetoranModel->terima_setoran($id);
        }else{
            $save = $this->SetoranModel->edit_setoran($id,$to_id);
        }
        $this->session->set_flashdata('message', 'success');
        redirect(BASE_URL.'setoran');
    }

    public function detail(){
        $this->session->set_userdata("active_trx_page", "setoran/detail/".$this->uri->segment(3));
        $user_id = $this->session->userdata("SesiUserId");
        $setor_id = $this->uri->segment(3);
        if ($setor_id == ""){
            redirect(BASE_URL.'setoran');
        }
        $data_setor = $this->SetoranModel->getSetoranDetail($setor_id);
        $data['total_uang'] = $data_setor->total;
        if ($data_setor->status == "New Setoran"){
            if ($user_id == $data_setor->to){
                $data['terima'] = true;
            }else{
                $data['terima'] = false;
            }
        }else{
            $data['terima'] = false;
        }
        $data['setoran_esge'] = $this->SetoranModel->getSetoranEsge($setor_id);
        $data['tarik_tunai'] = $this->SetoranModel->getTarikTunai($setor_id);
        $data['titip_transfer'] = $this->SetoranModel->getTitipTransfer($setor_id);
        $data['tektaya'] = $this->SetoranModel->getTektaya($setor_id);
        $data['penjualan'] = $this->SetoranModel->getPenjualan($setor_id);
        $data['jet'] = $this->SetoranModel->getJet($setor_id);
        $data['trx_lain'] = $this->SetoranModel->getTrxLain($setor_id);
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "setoran";
        $data['kode_setor'] = $setor_id;
		$data['content'] = 'setoran/detail';
		$this->load->view('template',$data);
    }

    
    function getSetoran(){

        $data = array();
        $a = new stdClass();
        $a->code = '78173813';
        $a->name = 'Ajeng';
        $a->total = 2150000;
        $a->time = '10:11:00';
        $a->status = 'New Setoran';
        array_push($data, $a);

        $a = new stdClass();
        $a->code = '21928773';
        $a->name = 'Doni';
        $a->total = 2150000;
        $a->time = '10:11:00';
        $a->status = 'Success';
        array_push($data, $a);

        $a = new stdClass();
        $a->code = '66167332';
        $a->name = 'Doni';
        $a->total = 3150000;
        $a->time = '09:11:00';
        $a->status = 'Success';
        array_push($data, $a);

        $a = new stdClass();
        $a->code = '21928773';
        $a->name = 'Doni';
        $a->total = 2150000;
        $a->time = '10:11:00';
        $a->status = 'Success';
        array_push($data, $a);

        $a = new stdClass();
        $a->code = '66167332';
        $a->name = 'Doni';
        $a->total = 3150000;
        $a->time = '09:11:00';
        $a->status = 'Success';
        array_push($data, $a);
        $a = new stdClass();
        $a->code = '21928773';
        $a->name = 'Doni';
        $a->total = 2150000;
        $a->time = '10:11:00';
        $a->status = 'Success';
        array_push($data, $a);

        $a = new stdClass();
        $a->code = '66167332';
        $a->name = 'Doni';
        $a->total = 3150000;
        $a->time = '09:11:00';
        $a->status = 'Success';
        array_push($data, $a);

        return $data;
    }
}