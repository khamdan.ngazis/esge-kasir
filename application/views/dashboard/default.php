
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="margin-top:-15px;">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Dashboard</h3>
                  <div class="float-sm-right">
                    <p ><?php echo date('d M Y');?></p>
                  </div>
                </div>
              </div>
              <div class="card-body">
              
              <h5 class="mb-2">Informasi Transaksi</h5>
              <?php 
                  foreach ($user_balance as $key => $value) {
                    if ($value->id == $this->session->userdata("SesiUserId")){
              ?>
                <div class="row">
                  <div class="col-md-6">
                    <div class="small-box bg-success">
                      <div class="inner">
                        <h3><?php echo $value->name; ?></h3>
                        <p>Total Uang<br>
                          Rp <?php echo number_format($value->balance); ?></p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                      </div>
                      <a href="#" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
                  <?php } }?>
                <div class="row">
                <?php if ( $this->session->userdata("SesiRole") == "Owner"){ ?>
                
                  <?php 
                    foreach ($user_balance as $key => $value) {
                      if ($value->id != $this->session->userdata("SesiUserId")){
                  ?>
                  <div class="col-md-4">
                    <div class="small-box bg-info">
                      <div class="inner">
                        <h3><?php echo $value->name; ?></h3>
                        <p>Total Uang<br>
                          Rp <?php echo number_format($value->balance); ?></p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                      </div>
                      <a href="#" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                      </a>
                    </div>
                  </div>
                  <?php } } }?>
                  <?php if ( $this->session->userdata("SesiRole") == "Kasir"){ 
                     foreach ($user_balance as $key => $value) {
                      if ($value->id != $this->session->userdata("SesiUserId") && $value->role != 'Owner'){
                  ?>
                  <div class="col-md-4">
                    <div class="small-box bg-info">
                      <div class="inner">
                        <h3><?php echo $value->name; ?></h3>
                        <p>Total Uang<br>
                          Rp <?php echo number_format($value->balance); ?></p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                      </div>
                      <a href="#" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                      </a>
                    </div>
                  </div>
                  <?php } } }?>
                </div>
                <h5 class="mb-2">Informasi Saldo</h5>
                <div class="row">
                  <?php foreach ($account_balance as $key => $value) {
                    # code...
                  ?>
                  <div class="col-md-3">
                    <div class="small-box <?php echo $value->background; ?>">
                      <div class="inner">
                  <h3 <?php if ($value->background == 'bg-warning'){ ?> style="color:#ffffff;" <?php } ?>><?php echo $value->name ?></h3>

                        <p  <?php if ($value->background == 'bg-warning'){ ?> style="color:#ffffff;" <?php } ?>>Rp. <?php echo number_format($value->balance); ?></p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-bank"></i>
                      </div>
                      <a href="#" class="small-box-footer">
                        <span  <?php if ($value->background == 'bg-warning'){ ?> style="color:#ffffff;" <?php } ?> <?php if ($value->background == 'bg-default'){ ?> style="color:#000;" <?php } ?>>More info</span> <i class="fas fa-arrow-circle-right"  style="color:#ffffff;"></i>
                      </a>
                    </div>
                  </div>
                  <?php } ?>
                  </div>
                </div>
               
              </div>
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
  </div>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->
