<?php
class transaction extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("TransactionModel");
        $this->load->model("UserModel");
        isLogin();
    }

    public function index(){
        $this->session->set_userdata("active_trx_page", "transaction");
        $user_id = $this->session->userdata("SesiUserId");
       // $data['banks'] =  $this->TransactionModel->getTotalTrxBank($user_id);
        $data['banks'] = $this->TransactionModel->getBank();
        $data['setoran_esge'] = $this->TransactionModel->getSetoranEsge($user_id);
        $data['tarik_tunai'] = $this->TransactionModel->getTarikTunai($user_id);
        $data['titip_transfer'] = $this->TransactionModel->getTitipTransfer($user_id);
        $data['accounts'] = $this->TransactionModel->getTotalTrxAccount( $user_id , 2);
        $data['tektaya'] = $this->TransactionModel->getTektaya($user_id);
        $data['penjualan'] = $this->TransactionModel->getPenjualan($user_id);
        $data['jet'] = $this->TransactionModel->getJet($user_id);
        $data['trx_lain'] = $this->TransactionModel->getTrxLain($user_id);
        $data['total_uang'] = $this->TransactionModel->getTotalTransaction($user_id)->total;
        $data['total_uang_setor'] = $this->TransactionModel->getTotalUangSetor($user_id)->total;
        $data['link_child'] = $this->uri->segment('1');
        $data['link'] = "transaksi";
		$data['content'] = 'transaction/default';
		$this->load->view('template',$data);
    }

    function getTotalTrxByBank(){
        $user_id = $this->session->userdata("SesiUserId");
        $bank_id = $this->input->post('bank_id');
        $banks =  $this->TransactionModel->getTotalTrxBank($user_id, $bank_id);
        $bank = array("total" => number_format($banks->total));
        echo json_encode($bank);
    }

    public function add_tektaya(){
        $data['link'] = "transaksi";
        $user_id = $this->session->userdata("SesiUserId");
        $data['accounts'] = $this->TransactionModel->getTotalTrxAccount( $user_id , 2);
		$this->load->view('transaction/add_tektaya',$data);
    }

    public function edit_tektaya(){
        $user_id = $this->session->userdata("SesiUserId");
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['accounts'] = $this->TransactionModel->getTotalTrxAccount( $user_id , 2);
        $data['trx'] = $this->TransactionModel->getSetoranEsgeDetail($trx_id, $account_id);
		$this->load->view('transaction/edit_tektaya',$data);
    }

    public function delete_tektaya(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getSetoranEsgeDetail($trx_id, $account_id);
		$this->load->view('transaction/delete_tektaya',$data);
    }
    public function add_penjualan(){
        $data['link'] = "transaksi";
		$this->load->view('transaction/add_penjualan',$data);
    }

    public function edit_penjualan(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getPenjualanDetail($account_id,$trx_id);
		$this->load->view('transaction/edit_penjualan',$data);
    }

    public function delete_penjualan(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getPenjualanDetail($account_id,$trx_id);
		$this->load->view('transaction/delete_penjualan',$data);
    }

    public function add_esge(){
        $data['link'] = "transaksi";
		$this->load->view('transaction/add_esge',$data);
    }
    public function edit_esge(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getSetoranEsgeDetail($trx_id, $account_id);
		$this->load->view('transaction/edit_esge',$data);
    }

    public function delete_esge(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getSetoranEsgeDetail($trx_id, $account_id);
		$this->load->view('transaction/delete_esge',$data);
    }

    public function save_esge() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $trx_time = $this->input->post('trx_time');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $income =  $this->input->post('income');
        $income = str_replace('.','',$income);
        $income = str_replace(',','',$income);

        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_transaction_account($user_id,1,$balance,$user_name,$transaction_name, $trx_time, $income);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_transaction_account($trx_account_id,$trx_id,$user_id,1,$balance,$user_name,$transaction_name, $trx_time, $income);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_transaction_account($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'esge');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }

    public function save_penjualan() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $price =  $this->input->post('price');
        $price = str_replace('.','',$price);
        $price = str_replace(',','',$price);
        $qty =  $this->input->post('qty');
        $total =  $this->input->post('total');
        $total = str_replace('.','',$total);
        $total = str_replace(',','',$total);

        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_transaction_sales($user_id,$total,$user_name,$transaction_name, $qty, $price);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_transaction_sales($trx_account_id,$trx_id,$user_id,$total,$user_name,$transaction_name, $qty, $price);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_transaction_sales($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'sales');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }

    public function save_tektaya() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $trx_time = $this->input->post('trx_time');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $income =  $this->input->post('income');
        $income = str_replace('.','',$income);
        $income = str_replace(',','',$income);
        $account_id =  $this->input->post('account');
        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_transaction_account($user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_transaction_account($trx_account_id,$trx_id,$user_id,$account_id,$balance,$user_name,$transaction_name, $trx_time, $income);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_transaction_account($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'tektaya');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }

    public function save_titip_transfer() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $trx_time = $this->input->post('trx_time');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $income =  $this->input->post('income');
        $income = str_replace('.','',$income);
        $income = str_replace(',','',$income);
        $bank_id = $this->input->post('bank');
        $trx_type = $this->input->post('trx_type');
        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_transaction_bank($user_id,$bank_id,$balance,$user_name,$transaction_name, $trx_time, $income, $trx_type);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_transaction_bank($trx_account_id,$trx_id,$user_id,$bank_id,$balance,$user_name,$transaction_name, $trx_time, $income,  $trx_type);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_transaction_bank($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'titip');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }

    public function add_titip_transfer(){
        $data['link'] = "transaksi";
        $data['banks'] =  $this->TransactionModel->getBank();
		$this->load->view('transaction/add_titip_transfer',$data);
    }

    public function add_jet(){
        $data['link'] = "transaksi";
		$this->load->view('transaction/add_jet',$data);
    }

    public function add_setoran_jet(){
        $data['link'] = "transaksi";
		$this->load->view('transaction/add_setoran_jet',$data);
    }

    public function edit_jet(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getJetDetail($account_id,$trx_id);
		$this->load->view('transaction/edit_jet',$data);
    }

    public function edit_trx_lain(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getTrxLainDetail($trx_id,$account_id);
		$this->load->view('transaction/edit_trx_lain',$data);
    }

    public function delete_trx_lain(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getTrxLainDetail($trx_id,$account_id);
		$this->load->view('transaction/delete_trx_lain',$data);
    }
    public function delete_jet(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['trx'] = $this->TransactionModel->getJetDetail($account_id,$trx_id);
		$this->load->view('transaction/delete_jet',$data);
    }

    public function save_jet() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $trx_type = $this->input->post('trx_type');
        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_transaction_courier($user_id,$balance,$trx_type,$user_name,$transaction_name);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_transaction_courier($trx_account_id,$trx_id,$user_id,$balance,$trx_type,$user_name,$transaction_name);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_transaction_courier($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'jet');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }

    public function save_trx_lain() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $trx_type = $this->input->post('trx_type');
        $note = $this->input->post('note');
        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_transaction_lain($user_id,$balance,$trx_type,$user_name,$transaction_name,$note);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_transaction_lain($trx_account_id,$trx_id,$user_id,$balance,$trx_type,$user_name,$transaction_name,$note);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_transaction_lain($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'lain');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }
    
    public function add_tarik_tunai(){
        $data['link'] = "transaksi";
        $data['banks'] =  $this->TransactionModel->getBank();
		$this->load->view('transaction/add_tarik_tunai',$data);
    }

    public function add_trx_lain(){
        $data['link'] = "transaksi";
		$this->load->view('transaction/add_trx_lain',$data);
    }

    public function save_tarik_tunai() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name =  $this->input->post('name');
        $trx_time = $this->input->post('time');
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $income =  $this->input->post('income');
        $income = str_replace('.','',$income);
        $income = str_replace(',','',$income);
        $bank_id = $this->input->post('bank');
        $trx_type = "Tarik Tunai";
        $trx_account_id = $this->input->post('account_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');
        if ( $action == "add"){
            $save = $this->TransactionModel->add_tarik_tunai($user_id,$bank_id,$balance,$user_name,$transaction_name, $trx_time, $income, $trx_type);
        }
        if ( $action == "edit"){
            $save = $this->TransactionModel->edit_tarik_tunai($trx_account_id,$trx_id,$user_id,$bank_id,$balance,$user_name,$transaction_name, $trx_time, $income,  $trx_type);
        }
        if ( $action == "delete"){
            $save = $this->TransactionModel->delete_tarik_tunai($trx_account_id,$trx_id);
        }
        $this->session->set_flashdata('tab', 'tarik');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }

    public function edit_tarik_tunai(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['banks'] =  $this->TransactionModel->getBank();
        $data['trx'] = $this->TransactionModel->getTitipTransferDetail($trx_id, $account_id);
		$this->load->view('transaction/edit_tarik_tunai',$data);
    }
    public function edit_titip_transfer(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['banks'] =  $this->TransactionModel->getBank();
        $data['trx'] = $this->TransactionModel->getTitipTransferDetail($trx_id, $account_id);
		$this->load->view('transaction/edit_titip_transfer',$data);
    }

    public function delete_tarik_tunai(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['banks'] =  $this->TransactionModel->getBank();
        $data['trx'] = $this->TransactionModel->getTitipTransferDetail($trx_id, $account_id);
		$this->load->view('transaction/delete_tarik_tunai',$data);
    }

    public function delete_titip_transfer(){
        $data['link'] = "transaksi";
        $account_id = $this->input->post('id');
        $trx_id = $this->input->post('trx_id');
        $data['banks'] =  $this->TransactionModel->getBank();
        $data['trx'] = $this->TransactionModel->getTitipTransferDetail($trx_id, $account_id);
		$this->load->view('transaction/delete_titip_transfer',$data);
    }

    public function add_setoran(){
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $role = $this->session->userdata("SesiRole");
        if ($role == "Customer Service"){
            $role_setor = "Kasir";
        }
        if($role == "Kasir"){
            $role_setor = "Owner";
        }
        $data['link'] = "transaksi";
        $data['role'] = $role;
        $data['users'] = $this->UserModel->getUserbyRole($role_setor);
        $data['total'] = $this->TransactionModel->getTotalUangBelumSetor($user_id)->total;
		$this->load->view('transaction/add_setoran',$data);
    }

    public function save_setoran(){
        $user_id = $this->session->userdata("SesiUserId");
        $to_id = $this->input->post('setor_id');
        $save = $this->TransactionModel->add_setoran($user_id,$to_id);
        $this->session->set_flashdata('message', 'success');
        redirect(BASE_URL.'setoran');
    }
   
    public function add_trx_transfer_uang(){
        $user_id = $this->session->userdata("SesiUserId");
        $data['link'] = "transaksi";
        $data['users'] = $this->UserModel->getAllUser();
        $data['total'] = $this->TransactionModel->getTotalUangBelumSetor($user_id)->total;
        $this->load->view('transaction/add_trx_transfer_uang',$data);
    }

    public function save_trx_transfer_uang() {
        $user_id = $this->session->userdata("SesiUserId");
        $user_name = $this->session->userdata("SesiName");
        $transaction_name = "#Transfer Uang";
        $balance =  $this->input->post('balance');
        $balance = str_replace('.','',$balance);
        $balance = str_replace(',','',$balance);
        $keluar = -1;
        $masuk = 1;
        $transferTo = $this->input->post('transfer_to');
        $UserDetail = $this->UserModel->getUser($transferTo);
        $toName = $UserDetail->name;
        $note_keluar = $user_name." Transfer ke ".$toName;
        $note_masuk = $toName." Terima Transfer Dari ".$user_name;
 
        $save_keluar = $this->TransactionModel->add_transaction_lain($user_id,$balance,$keluar,$user_name,$transaction_name,$note_keluar);
        $save_masuk = $this->TransactionModel->add_transaction_lain($transferTo,$balance,$masuk,$toName,$transaction_name,$note_masuk);
        
        $this->session->set_flashdata('tab', 'lain');
        $this->session->set_flashdata('message', 'success');
        $link = $this->session->userdata("active_trx_page");
        redirect(BASE_URL.$link);
    }
}