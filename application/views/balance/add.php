<div class="modal-header bg-info">
    <h4 class="modal-title">Tambah Saldo <span id="form_balance_name"><?php echo $name; ?></span></h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
</div>
<form action="<?php echo BASE_URL; ?>balance/save" method="POST" id="form_add">
<input type="hidden" name="action" value="add">
<input type="hidden" name="type" value="<?php echo $type; ?>">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<div class="modal-body">
    <div class="col-md-12">
    <?php if ($type == "bank"){ ?>
        <div class="form-group">
            <label >Jenis Transaksi</label>
            <select class="form-control" name="trx_type">
                <option value="1">Saldo Masuk</option>
                <option value="-1">Saldo Keluar</option>
            </select>
        </div>

    <?php } ?>
        <div class="form-group">
            <label >Jumlah</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </div>
                <input type="text" class="form-control" id="balance" name="balance" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digitsOptional': false ">
            </div>
        </div>
        <?php if ($type == "bank"){ ?>
        <div class="form-group">
            <label >Keterangan</label>
            <input type="text" class="form-control" id="note" name="note" >
        </div>
        <?php } ?>
    </div>
    <div class="modal-footer">
        <button type="submit" id="submit" class="btn btn-info btn-sm">Save</button>
    </div>

</div>
</form>
<script>
    function onPrepre(){
        $("#balance").inputmask();
    }
    $("#form_add").submit(function(){
        $('#submit').html('Loading...');
        $('#submit').attr('disabled', 'disabled');
    });

</script>