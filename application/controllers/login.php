<?php
class login extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("UserModel");
        date_default_timezone_set('Asia/Jakarta');
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Pragma: no-cache");
    }

    public function index(){
		$this->load->view('login/default');
    }

    public function process (){
        $UserName = strtoupper($this->input->post('Username'));
        $Password = $this->input->post('Password');
        $ret = $this->UserModel->getUserLogin($UserName);
        if($ret == NULL) {
            $message = "<span class='text-danger'> User doesnt exist </span>";
            $sesion = array(
                'message'  => $message
            );
            $this->session->set_flashdata($sesion);
            redirect(BASE_URL."login");
        }
        if($ret->password !== hash("sha256", $Password)) {
            $message = "<span class='text-danger'> Wrong Password  </span>";
            $sesion = array(
                'message'  => $message
            );
            $this->session->set_flashdata($sesion);
            redirect(BASE_URL."login");
        }
        $sesion = array(
            'LoggedIn'  => TRUE,
            'SesiUserId'     => $ret->id,
            'SesiUserName' => $UserName,
            'SesiRole' => $ret->role,
            'SesiName' => $ret->name
        );
        
        $this->session->set_userdata($sesion);
        
        redirect(BASE_URL."dashboard");
    }
    function logout(){
        $this->session->sess_destroy();
		redirect(BASE_URL.'login', 'refresh');
	}
}