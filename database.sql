-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2020 at 03:08 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pos_esge`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_setoran` (IN `in_from_user_id` INTEGER(10), IN `in_to_user_id` INTEGER(10))  BEGIN
	DECLARE setor_id int;

	insert into trx_setoran (`from`,`to`,status) value (in_from_user_id, in_to_user_id, 10);
	SET setor_id = LAST_INSERT_ID();
	INSERT INTO trx_setoran_details SELECT null as new_id, setor_id as s_id , id FROM trx_transaction where
	user_id = in_from_user_id and date(create_at) = curdate() and id not in (select trx_id from trx_setoran_details); 
    
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_transaction_account` (IN `in_user_id` INTEGER(10), IN `in_account_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_time` VARCHAR(10), IN `is_transaction` BOOLEAN, IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10))  BEGIN
	DECLARE old_balance int;
    DECLARE new_balance int;
    DECLARE in_trx_id int;
    DECLARE in_trx_account_id int;
    select balance into old_balance from `mtr_accounts` where id = in_account_id;
    SET new_balance = old_balance + (in_balance * in_trx_type);

    if (is_transaction)
    THEN 
          insert into `trx_account` 
                (account_id, name , trx_type , amount , trx_time,  created_by , updated_by) value
                (in_account_id, in_name, in_trx_type, in_balance, in_trx_time, in_user_name, in_user_name);
            
          SET in_trx_account_id = LAST_INSERT_ID();
            
          update `mtr_accounts` set balance = new_balance where id = in_account_id;
          insert into trx_transaction ( name , trx_type , amount , trx_time,  created_by , updated_by, user_id) value
                      (in_name, in_trx_transaction_type, in_transaction_balance, in_trx_time, in_user_name, in_user_name, in_user_id);
          SET in_trx_id = LAST_INSERT_ID();
          insert into `trx_transaction_account` (trx_id, trx_account_id) value
                      (in_trx_id, in_trx_account_id);
    else 
          insert into `trx_account` 
              (account_id, name , trx_type , amount , trx_time,  created_by , updated_by) value
              (in_account_id, in_name, in_trx_type, in_balance, in_trx_time, in_user_name, in_user_name);
                   
          update `mtr_accounts` set balance = new_balance where id = in_account_id;  
    END IF;
  	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_transaction_bank` (IN `in_user_id` INTEGER(10), IN `in_account_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_time` VARCHAR(10), IN `is_transaction` BOOLEAN, IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10), IN `transaction_type` VARCHAR(100))  BEGIN
	DECLARE old_balance int;
    DECLARE new_balance int;
    DECLARE trx_id int;
    DECLARE trx_account_id int;

    select balance into old_balance from `mtr_bank_accounts` where id = in_account_id;
    SET new_balance = old_balance + (in_balance * in_trx_type);

    	insert into `trx_bank` 
        	(bank_id, name , trx_type , amount , trx_time,  created_by , updated_by, transaction) value
        	(in_account_id, in_name, in_trx_type, in_balance, in_trx_time, in_user_name, in_user_name,transaction_type );
        
        SET trx_account_id = LAST_INSERT_ID();
        
        update `mtr_bank_accounts` set balance = new_balance where id = in_account_id;
        
        if (is_transaction)
        	THEN 
        		insert into trx_transaction ( name , trx_type , amount , trx_time,  created_by , updated_by, user_id) value
                	(in_name, in_trx_transaction_type, in_transaction_balance, in_trx_time, in_user_name, in_user_name, in_user_id);
                SET trx_id = LAST_INSERT_ID();
                insert into `trx_transaction_bank` (trx_id, trx_bank_id) value
                	(trx_id, trx_account_id);
            END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_transaction_courier` (IN `in_user_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10))  BEGIN

    DECLARE trx_id int;
    DECLARE trx_courier_id int;

    	insert into `trx_courier` 
        	( name , trx_type , amount ,  created_by , updated_by) value
        	(in_name, in_trx_type, in_balance, in_user_name, in_user_name);
        
        SET trx_courier_id = LAST_INSERT_ID();
        
        insert into trx_transaction ( name , trx_type , amount , trx_time,  created_by , updated_by, user_id) value
            (in_name, in_trx_transaction_type, in_transaction_balance, '', in_user_name, in_user_name, in_user_id);
        SET trx_id = LAST_INSERT_ID();
        insert into `trx_transaction_courier` (trx_id, trx_courier_id) value
            (trx_id, trx_courier_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_transaction_lain` (IN `in_user_id` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10), IN `in_note` VARCHAR(255))  BEGIN

    DECLARE trx_id int;
        
    insert into trx_transaction ( name , trx_type , amount , trx_time,  created_by , updated_by, user_id) value
        (in_name, in_trx_transaction_type, in_transaction_balance, '', in_user_name, in_user_name, in_user_id);
    SET trx_id = LAST_INSERT_ID();
    insert into `trx_transaction_lain` (trx_id, note) value
        (trx_id, in_note);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_transaction_sales` (IN `in_user_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10), IN `in_qty` INTEGER(4), IN `in_price` INTEGER(100))  BEGIN
    DECLARE trx_id int;
    DECLARE trx_sales_id int;
    	insert into `trx_sales` 
        	( name , trx_type , price , qty, total, created_by , updated_by) value
        	( in_name, in_trx_type, in_price, in_qty, in_balance, in_user_name,in_user_name);
        
        SET trx_sales_id = LAST_INSERT_ID();

        insert into trx_transaction ( name , trx_type , amount , trx_time,  created_by , updated_by, user_id) value
            (in_name, in_trx_transaction_type, in_transaction_balance, "", in_user_name, in_user_name, in_user_id);
        SET trx_id = LAST_INSERT_ID();
        insert into `trx_transaction_sales` (trx_id, trx_sales_id) value
            (trx_id, trx_sales_id);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_transaction_account` (IN `in_trx_account_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `is_transaction` BOOLEAN)  BEGIN
	DECLARE in_account_id int;
    DECLARE in_amount int;
	DECLARE old_balance int;
    DECLARE new_balance int;

	select account_id,amount  into in_account_id, in_amount from trx_account where id = in_trx_account_id;
    select balance into old_balance from `mtr_accounts` where id = in_account_id;
    if (in_trx_type = 1) THEN
        SET new_balance = old_balance - in_amount;
    else
        SET new_balance = old_balance + in_amount;
    end if;
    
    if (is_transaction)
    THEN 
          
          update `mtr_accounts` set balance = new_balance where id = in_account_id;
          
          delete from trx_transaction_account where trx_id = in_trx_id and trx_account_id = in_trx_account_id;
          
          delete from trx_account where id = in_trx_account_id;
          
          delete from trx_transaction where id = in_trx_id;

    else 
          delete from trx_account where id = in_trx_account_id;

          update `mtr_accounts` set balance = new_balance where id = in_account_id;
    END IF;
  	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_transaction_bank` (IN `in_trx_account_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `is_transaction` BOOLEAN)  BEGIN
	DECLARE in_account_id int;
    DECLARE in_amount int;
	DECLARE old_balance int;
    DECLARE new_balance int;

	select bank_id,amount  into in_account_id, in_amount from trx_bank where id = in_trx_account_id;
    select balance into old_balance from `mtr_bank_accounts` where id = in_account_id;
    if (in_trx_type = 1) THEN
        SET new_balance = old_balance - in_amount;
    else
        SET new_balance = old_balance + in_amount;
    end if;
    
    if (is_transaction)
    THEN 
          
          update `mtr_bank_accounts` set balance = new_balance where id = in_account_id;
          
          delete from trx_transaction_bank where trx_id = in_trx_id and trx_bank_id = in_trx_account_id;
          
          delete from `trx_bank` where id = in_trx_account_id;
          
          delete from trx_transaction where id = in_trx_id;

    else 
          delete from trx_bank where id = in_trx_account_id;

          update `mtr_bank_accounts` set balance = new_balance where id = in_account_id;
    END IF;
  	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_transaction_courier` (IN `in_trx_account_id` INTEGER(10), IN `in_trx_id` INTEGER(10))  BEGIN
          
    delete from trx_transaction_courier where trx_id = in_trx_id and trx_courier_id = in_trx_account_id;
          
    delete from `trx_courier` where id = in_trx_account_id;
          
    delete from trx_transaction where id = in_trx_id;

  	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_transaction_lain` (IN `in_trx_lain_id` INTEGER(10), IN `in_trx_id` INTEGER(10))  BEGIN

    delete from trx_transaction_lain where id = in_trx_lain_id; 
    delete from trx_transaction where id = in_trx_id;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_transaction_sales` (IN `in_trx_account_id` INTEGER(10), IN `in_trx_id` INTEGER(10))  BEGIN
          
    delete from trx_transaction_sales where trx_id = in_trx_id and trx_sales_id = in_trx_account_id;
          
    delete from `trx_sales` where id = in_trx_account_id;
          
    delete from trx_transaction where id = in_trx_id;

  	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_transaction_account` (IN `in_trx_account_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_user_id` INTEGER(10), IN `in_account_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_time` VARCHAR(10), IN `is_transaction` BOOLEAN, IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10))  BEGIN
	DECLARE old_amount int;
    DECLARE new_amount int;
	DECLARE old_balance int;
    DECLARE new_balance int;

    select amount into old_amount from `trx_account` where id = in_trx_account_id;
    select balance into old_balance from `mtr_accounts` where id = in_account_id;
    if (in_trx_type = 1) THEN
     	set new_amount = in_balance - old_amount;
        SET new_balance = old_balance + new_amount;
    else
    	set new_amount = (in_trx_type * old_amount) + in_balance;
         SET new_balance = old_balance + (in_trx_type * new_amount);
    end if;
    

    if (is_transaction)
    THEN 
          update `trx_account` set amount = in_balance, updated_by = in_user_name, 
          trx_time = in_trx_time,  name = in_name where id = in_trx_account_id;

          update `mtr_accounts` set balance = new_balance where id = in_account_id;
          update trx_transaction set name = in_name, amount = in_transaction_balance, 
          trx_time = in_trx_time, updated_by = in_user_name where id = in_trx_id;
    else 
          update `trx_account` set amount = in_balance, updated_by = in_user_name, 
          trx_time = in_trx_time,  name = in_name where id = in_trx_account_id;

          update `mtr_accounts` set balance = new_balance where id = in_account_id; 
    END IF;
  	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_transaction_bank` (IN `in_trx_bank_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_user_id` INTEGER(10), IN `in_account_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_time` VARCHAR(10), IN `is_transaction` BOOLEAN, IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10), IN `transaction_type` VARCHAR(100))  BEGIN
	DECLARE old_balance int;
    DECLARE new_balance int;
    DECLARE trx_old_account_id int;
    DECLARE old_amount int;
    DECLARE new_amount int;

	select bank_id into trx_old_account_id from trx_bank where id = in_trx_bank_id;
    select amount into old_amount from `trx_bank` where id = in_trx_bank_id;
    if (trx_old_account_id = in_account_id) THEN
        select balance into old_balance from `mtr_bank_accounts` where id = in_account_id;
        if (in_trx_type = 1) THEN
     		set new_amount = in_balance -  old_amount;
        	SET new_balance = old_balance + new_amount;
   		else
    		set new_amount = (in_trx_type * old_amount) + in_balance;
        	SET new_balance = old_balance + (in_trx_type * new_amount);
    	end if;

        update `mtr_bank_accounts` set balance = new_balance where id = in_account_id;
    else 
    	select balance into old_balance from `mtr_bank_accounts` where id = trx_old_account_id;
         if (in_trx_type = 1) THEN
			SET new_balance = old_balance - old_amount;
         else 
            SET new_balance = old_balance + old_amount;
         end if;
        update `mtr_bank_accounts` set balance = new_balance where id = trx_old_account_id;
        
        select balance into old_balance from `mtr_bank_accounts` where id = in_account_id;
    	SET new_balance = old_balance + (in_balance * in_trx_type);
        update `mtr_bank_accounts` set balance = new_balance where id = in_account_id;
    end if;

	update trx_bank set bank_id = in_account_id, name = in_name, amount = in_balance, 
    trx_time = in_trx_time, updated_by = in_user_name, transaction = transaction_type 
    where id = in_trx_bank_id;
    
    if (is_transaction) THEN 
    	update trx_transaction set name = in_name, amount = in_transaction_balance, 
        trx_time = in_trx_time, updated_by = in_user_name where id = in_trx_id;
    END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_transaction_courier` (IN `in_trx_courier_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_user_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10))  BEGIN

    update trx_courier set name = in_name , amount = in_balance, updated_by = in_user_name
    where id = in_trx_courier_id;

    update trx_transaction set name = in_name, amount = in_transaction_balance,
    updated_by = in_user_name where id = in_trx_id;
        

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_transaction_lain` (IN `in_trx_lain_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_user_id` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10), IN `in_note` VARCHAR(255))  BEGIN

       
    update trx_transaction set name = in_name , trx_type = in_trx_transaction_type  , amount = in_transaction_balance
    	, trx_time = '', updated_by = in_user_name where id = in_trx_id;
    update trx_transaction_lain set note = in_note  where id = in_trx_lain_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `edit_transaction_sales` (IN `in_trx_sales_id` INTEGER(10), IN `in_trx_id` INTEGER(10), IN `in_user_id` INTEGER(10), IN `in_balance` INTEGER(10), IN `in_trx_type` INTEGER(10), IN `in_user_name` VARCHAR(100), IN `in_name` VARCHAR(100), IN `in_trx_transaction_type` TINYINT, IN `in_transaction_balance` INTEGER(10), IN `in_qty` INTEGER(4), IN `in_price` INTEGER(100))  BEGIN
		update trx_sales set name = in_name, price = in_price, qty = in_qty ,
        total = in_balance, updated_by = in_user_name
        where id = in_trx_sales_id;

		update trx_transaction set name = in_name, amount = in_transaction_balance, 
        updated_by = in_user_name where id = in_trx_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `terima_setoran` (IN `in_setor_id` INTEGER(10))  BEGIN
	DECLARE user_to int;

	select `to` into user_to from trx_setoran where id = in_setor_id;
	update trx_setoran set status = 50 where id = in_setor_id;
	update trx_transaction set user_id = user_to where id in (select trx_id from trx_setoran_details where setoran_id = in_setor_id);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mtr_accounts`
--

CREATE TABLE `mtr_accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mtr_accounts`
--

INSERT INTO `mtr_accounts` (`id`, `name`, `balance`, `last_update`, `is_active`) VALUES
(1, 'RBR', 17340000, '2020-08-16 10:00:11', 1),
(2, 'TEKTAYA', 10204860, '2020-08-16 04:37:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mtr_bank_accounts`
--

CREATE TABLE `mtr_bank_accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) NOT NULL DEFAULT '1' COMMENT '1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `mtr_bank_accounts`
--

INSERT INTO `mtr_bank_accounts` (`id`, `name`, `balance`, `last_update`, `is_active`) VALUES
(1, 'BCA', 6680000, '2020-08-16 04:35:23', 1),
(2, 'BRI', 3100000, '2020-08-16 10:03:54', 1),
(3, 'BNI', 7900000, '2020-08-01 23:18:03', 1),
(4, 'PERMATA 1', 0, '2020-07-12 04:45:18', 1),
(5, 'PERMATA 2', 0, '2020-07-18 07:57:47', 1),
(6, 'MANDIRI', 0, '2020-07-12 04:45:39', 1),
(7, 'BJB', 0, '2020-07-12 04:46:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mtr_users`
--

CREATE TABLE `mtr_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT '' COMMENT '1 = owner\r\n2 = chasier\r\n3 = user',
  `name` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `mtr_users`
--

INSERT INTO `mtr_users` (`id`, `username`, `password`, `role`, `name`, `is_active`) VALUES
(1, 'administrator', 'cc979dd194fb8d069f53cc43f4d527ecfda8f45860196184318f82ed94b99fab', 'Owner', 'Admin', 1),
(2, 'Kasir', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'Kasir', 'Kasir', 1),
(3, 'customer.service', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 'Customer Service', 'Customer Service', 1),

-- --------------------------------------------------------

--
-- Table structure for table `trx_account`
--

CREATE TABLE `trx_account` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `trx_type` int(11) NOT NULL DEFAULT '1' COMMENT '1 = kredit,\r\n-1 = debet',
  `amount` int(11) NOT NULL DEFAULT '0',
  `trx_time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_account`
--


-- --------------------------------------------------------

--
-- Table structure for table `trx_bank`
--

CREATE TABLE `trx_bank` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `transaction` varchar(100) NOT NULL DEFAULT '',
  `trx_type` int(11) NOT NULL DEFAULT '1' COMMENT '1 = kredit,\r\n-1 = debet',
  `amount` int(11) NOT NULL DEFAULT '0',
  `trx_time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_bank`
--


-- --------------------------------------------------------

--
-- Table structure for table `trx_courier`
--

CREATE TABLE `trx_courier` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `trx_type` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_courier`
--


-- --------------------------------------------------------

--
-- Table structure for table `trx_sales`
--

CREATE TABLE `trx_sales` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) NOT NULL,
  `trx_type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_sales`
--


-- --------------------------------------------------------

--
-- Table structure for table `trx_setoran`
--

CREATE TABLE `trx_setoran` (
  `id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '10 = new\r\n20 = approve\r\n50 = cencel',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_setoran`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_setoran_details`
--

CREATE TABLE `trx_setoran_details` (
  `id` int(11) NOT NULL,
  `setoran_id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_setoran_details`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_transaction`
--

CREATE TABLE `trx_transaction` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `trx_type` int(11) NOT NULL DEFAULT '1',
  `trx_time` varchar(20) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_transaction`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_transaction_account`
--

CREATE TABLE `trx_transaction_account` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `trx_account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_transaction_account`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_transaction_bank`
--

CREATE TABLE `trx_transaction_bank` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `trx_bank_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_transaction_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_transaction_courier`
--

CREATE TABLE `trx_transaction_courier` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `trx_courier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_transaction_courier`
--


-- --------------------------------------------------------

--
-- Table structure for table `trx_transaction_lain`
--

CREATE TABLE `trx_transaction_lain` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_transaction_lain`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_transaction_sales`
--

CREATE TABLE `trx_transaction_sales` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `trx_sales_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `trx_transaction_sales`
--



--
-- Indexes for dumped tables
--

--
-- Indexes for table `mtr_accounts`
--
ALTER TABLE `mtr_accounts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mtr_bank_accounts`
--
ALTER TABLE `mtr_bank_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mtr_users`
--
ALTER TABLE `mtr_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `trx_account`
--
ALTER TABLE `trx_account`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `trx_account_fk1` (`account_id`);

--
-- Indexes for table `trx_bank`
--
ALTER TABLE `trx_bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_bank_fk1` (`bank_id`);

--
-- Indexes for table `trx_courier`
--
ALTER TABLE `trx_courier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trx_sales`
--
ALTER TABLE `trx_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trx_setoran`
--
ALTER TABLE `trx_setoran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_setoran_fk1` (`from`),
  ADD KEY `trx_setoran_fk2` (`to`);

--
-- Indexes for table `trx_setoran_details`
--
ALTER TABLE `trx_setoran_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trx_transaction`
--
ALTER TABLE `trx_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_transaction_fk1` (`user_id`);

--
-- Indexes for table `trx_transaction_account`
--
ALTER TABLE `trx_transaction_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_transaction_account_fk1` (`trx_id`),
  ADD KEY `trx_transaction_account_fk2` (`trx_account_id`);

--
-- Indexes for table `trx_transaction_bank`
--
ALTER TABLE `trx_transaction_bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_transaction_bank_fk1` (`trx_id`),
  ADD KEY `trx_transaction_bank_fk2` (`trx_bank_id`);

--
-- Indexes for table `trx_transaction_courier`
--
ALTER TABLE `trx_transaction_courier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_transaction_courier_fk1` (`trx_id`),
  ADD KEY `trx_transaction_courier_fk2` (`trx_courier_id`);

--
-- Indexes for table `trx_transaction_lain`
--
ALTER TABLE `trx_transaction_lain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_transaction_lain_fk1` (`trx_id`);

--
-- Indexes for table `trx_transaction_sales`
--
ALTER TABLE `trx_transaction_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trx_transaction_sales_fk1` (`trx_id`),
  ADD KEY `trx_transaction_sales_fk2` (`trx_sales_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mtr_accounts`
--
ALTER TABLE `mtr_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mtr_bank_accounts`
--
ALTER TABLE `mtr_bank_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mtr_users`
--
ALTER TABLE `mtr_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trx_account`
--
ALTER TABLE `trx_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `trx_bank`
--
ALTER TABLE `trx_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `trx_courier`
--
ALTER TABLE `trx_courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `trx_sales`
--
ALTER TABLE `trx_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `trx_setoran`
--
ALTER TABLE `trx_setoran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `trx_setoran_details`
--
ALTER TABLE `trx_setoran_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `trx_transaction`
--
ALTER TABLE `trx_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `trx_transaction_account`
--
ALTER TABLE `trx_transaction_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `trx_transaction_bank`
--
ALTER TABLE `trx_transaction_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `trx_transaction_courier`
--
ALTER TABLE `trx_transaction_courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `trx_transaction_lain`
--
ALTER TABLE `trx_transaction_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `trx_transaction_sales`
--
ALTER TABLE `trx_transaction_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `trx_account`
--
ALTER TABLE `trx_account`
  ADD CONSTRAINT `trx_account_fk1` FOREIGN KEY (`account_id`) REFERENCES `mtr_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_bank`
--
ALTER TABLE `trx_bank`
  ADD CONSTRAINT `trx_bank_fk1` FOREIGN KEY (`bank_id`) REFERENCES `mtr_bank_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_setoran`
--
ALTER TABLE `trx_setoran`
  ADD CONSTRAINT `trx_setoran_fk1` FOREIGN KEY (`from`) REFERENCES `mtr_users` (`id`),
  ADD CONSTRAINT `trx_setoran_fk2` FOREIGN KEY (`to`) REFERENCES `mtr_users` (`id`);

--
-- Constraints for table `trx_transaction`
--
ALTER TABLE `trx_transaction`
  ADD CONSTRAINT `trx_transaction_fk1` FOREIGN KEY (`user_id`) REFERENCES `mtr_users` (`id`);

--
-- Constraints for table `trx_transaction_account`
--
ALTER TABLE `trx_transaction_account`
  ADD CONSTRAINT `trx_transaction_account_fk1` FOREIGN KEY (`trx_id`) REFERENCES `trx_transaction` (`id`),
  ADD CONSTRAINT `trx_transaction_account_fk2` FOREIGN KEY (`trx_account_id`) REFERENCES `trx_account` (`id`);

--
-- Constraints for table `trx_transaction_bank`
--
ALTER TABLE `trx_transaction_bank`
  ADD CONSTRAINT `trx_transaction_bank_fk1` FOREIGN KEY (`trx_id`) REFERENCES `trx_transaction` (`id`),
  ADD CONSTRAINT `trx_transaction_bank_fk2` FOREIGN KEY (`trx_bank_id`) REFERENCES `trx_bank` (`id`);

--
-- Constraints for table `trx_transaction_courier`
--
ALTER TABLE `trx_transaction_courier`
  ADD CONSTRAINT `trx_transaction_courier_fk1` FOREIGN KEY (`trx_id`) REFERENCES `trx_transaction` (`id`),
  ADD CONSTRAINT `trx_transaction_courier_fk2` FOREIGN KEY (`trx_courier_id`) REFERENCES `trx_courier` (`id`);

--
-- Constraints for table `trx_transaction_lain`
--
ALTER TABLE `trx_transaction_lain`
  ADD CONSTRAINT `trx_transaction_lain_fk1` FOREIGN KEY (`trx_id`) REFERENCES `trx_transaction` (`id`);

--
-- Constraints for table `trx_transaction_sales`
--
ALTER TABLE `trx_transaction_sales`
  ADD CONSTRAINT `trx_transaction_sales_fk1` FOREIGN KEY (`trx_id`) REFERENCES `trx_transaction` (`id`),
  ADD CONSTRAINT `trx_transaction_sales_fk2` FOREIGN KEY (`trx_sales_id`) REFERENCES `trx_sales` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
